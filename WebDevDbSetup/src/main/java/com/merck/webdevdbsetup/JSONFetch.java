package com.merck.webdevdbsetup;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.kohsuke.github.GHContent;
import org.kohsuke.github.GHRepository;
import org.kohsuke.github.GitHub;

/**
 * This class is using the github api to fetch all of the necessary json
 * files to fill our database
 *
 * @author 1839499
 */
public class JSONFetch {

    private static String username = "";
    private static String password = "";
    private static DatabaseFiller dbInsert;

    /**
     * This method connects to the GitHub api and loads our forked repo then
     * downloads all of the json files
     *
     * @author 1839499
     */
    public static void fetch() {
        try {
            GitHub github = GitHub.connectAnonymously();
            GHRepository repo = github.getRepositoryById(454246672);
            List<GHContent> pokemonFiles = repo.getDirectoryContent("cards/en");
            List<GHContent> setFiles = repo.getDirectoryContent("sets");
            setJSONConvert(setFiles);
            cardJSONConvert(pokemonFiles);
        } catch (IOException e) {

        }
    }

    /**
     * This method takes our Card GHContent list and converts it into JSON files
     * and then individual JSON objects
     *
     * @param list
     * @author 1839499
     */
    private static void cardJSONConvert(List<GHContent> list) {
        for (int i = 0; i < list.size(); i++) {
            try {
                InputStream iStream = list.get(i).read();
                JSONParser jsonParser = new JSONParser();
                JSONArray jsonArray = (JSONArray) jsonParser.parse(new InputStreamReader(iStream, "UTF-8"));
                for (int j = 0; j < jsonArray.size(); j++) {
                    JSONObject holder = (JSONObject) jsonParser.parse(jsonArray.get(j).toString());
                    cardJSONRefactor(holder);
                }
            } catch (IOException | ParseException err) {
                System.out.println("Unable to convert card");
            }
        }
        System.out.println("All cards added");
    }

    /**
     * This method breaks down the inputted JSON object into individual
     * properties and adds them to a property list to be added to the database
     *
     * @param list
     * @author 1839499
     */
    private static void cardJSONRefactor(JSONObject input) {
        ArrayList<String> cardProperties = new ArrayList<>();
        String id = (String) input.get("id");
        String setId = id.substring(0, id.indexOf("-"));
        cardProperties.add(id);
        cardProperties.add(setId);
        cardProperties.add((String) input.get("name"));
        cardProperties.add((String) input.get("supertype"));
        checkNull((JSONArray) input.get("subtypes"), cardProperties, "No Subtype");
        checkNull((JSONArray) input.get("types"), cardProperties, "No Type");
        checkNull((JSONArray) input.get("evolvesTo"), cardProperties, "No Evolution");
        checkNull((String) input.get("evolvesFrom"), cardProperties, "No Pre-Evolution");
        checkNull((String) input.get("rarity"), cardProperties, "No-Rarity");
        checkText((String) input.get("flavorText"), ((JSONArray) input.get("rules")), cardProperties);
        checkNull((JSONArray) input.get("nationalPokedexNumbers"), cardProperties, "No Pokedex Entry");
        checkNull((String) input.get("artist"), cardProperties, "Pokémon-TCG");
        cardProperties.add(null);
        try {
            dbInsert.insertCard(cardProperties);
        } catch (SQLException sqle) {
            sqle.printStackTrace();
            System.out.println("Unable to add card");
        }
    }

    /**
     * This method takes our Set GHContent list and converts it into JSON files
     * and then individual JSON objects
     *
     * @param list
     * @author 1839499
     */
    private static void setJSONConvert(List<GHContent> list) {
        for (int i = 0; i < list.size(); i++) {
            try {
                InputStream iStream = list.get(i).read();
                JSONParser jsonParser = new JSONParser();
                JSONArray jsonArray = (JSONArray) jsonParser.parse(new InputStreamReader(iStream, "UTF-8"));
                for (int j = 0; j < jsonArray.size(); j++) {
                    JSONObject holder = (JSONObject) jsonParser.parse(jsonArray.get(j).toString());
                    setJSONRefactor(holder);
                }
            } catch (IOException | ParseException err) {
                System.out.println("unable to convert sets");
            }
        }
        System.out.println("all sets added");
    }

    /**
     * This method breaks down the inputted JSON object into individual
     * properties adds them to a property list to be added to the database
     *
     * @param list
     * @author 1839499
     */
    private static void setJSONRefactor(JSONObject input) {
        ArrayList<String> setProperties = new ArrayList<>();
        setProperties.add((String) input.get("id"));
        setProperties.add((String) input.get("name"));
        setProperties.add((String) input.get("series"));
        setProperties.add((long) input.get("total") + "");
        setProperties.add((String) input.get("releaseDate"));
        setProperties.add(null);
        try {
            dbInsert.insertSet(setProperties);
        } catch (SQLException sqle) {
            System.out.println("Unable to add set");
        }
    }

    /**
     * This is a helper method to handle having multiple entries for one
     * property on a card
     *
     * @param input
     * @param cardProperties
     * @author 1839499
     */
    private static void multiCheck(JSONArray input, ArrayList<String> cardProperties) {
        try {
            if (input.size() > 1) {
                StringBuilder build = new StringBuilder();
                for (int i = 0; i < input.size(); i++) {
                    String holder = input.get(i).toString();
                    if (i + 1 == input.size()) {
                        build.append(holder);
                    } else {
                        build.append(holder).append(".");
                    }
                }
                cardProperties.add(build.toString());
            } else {
                try {
                    cardProperties.add(input.get(0).toString());
                } catch (ClassCastException cce) {
                    JSONParser jsonParser = new JSONParser();
                    long holder = (long) jsonParser.parse(input.get(0).toString());
                    cardProperties.add(holder + "");
                }
            }
        } catch (ParseException pe) {

        }

    }

    /**
     * This is a helper method to facilitate checking for null values in the
     * JSONArray inputs
     *
     * @param arrayInput
     * @param cardProperties
     * @author 1839499
     */
    private static void checkNull(JSONArray arrayInput, ArrayList<String> cardProperties, String replacement) {
        if (arrayInput != null) {
            multiCheck(arrayInput, cardProperties);
        } else {
            cardProperties.add(replacement);
        }
    }

    /**
     * This is a helper method to facilitate checking for null values in the
     * JSONObject(converted to strings) inputs
     *
     * @param input
     * @param cardProperties
     * @param replacement
     * @author 1839499
     */
    private static void checkNull(String input, ArrayList<String> cardProperties, String replacement) {
        if (input == null) {
            cardProperties.add(replacement);
        } else {
            cardProperties.add(input);

        }
    }

    /**
     * This is a helper method to determine if a card has text on it or not
     *
     * @param flavorText
     * @param trainerText
     * @param cardProperties
     * @author 1839499
     */
    private static void checkText(String flavorText, JSONArray trainerText, ArrayList<String> cardProperties) {
        if (flavorText == null && trainerText != null) {
            multiCheck(trainerText, cardProperties);
        } else if (flavorText != null) {
            cardProperties.add(flavorText);
        } else {
            cardProperties.add("No Card Text");
        }
    }

    /**
     * This method handles login into our database
     * @author 18394999
     */
    public static void dbCredentials() {
        try (Scanner reader = new Scanner(System.in)) {
            while (username.equals("")) {
                System.out.println("Insert db username");
                username = reader.next();
            }
            while (password.equals("")) {
                System.out.println("insert db password");
                password = reader.next();
            }
        }
        dbInsert = new DatabaseFiller("jdbc:postgresql://pokemoncarddb.postgres.database.azure.com:5432/pokemoncarddb?", username + "@pokemoncarddb.postgres.database.azure.com", password);
    }

    /**
     * Method to run program
     *
     * @param args
     */
    public static void main(String[] args) {
        dbCredentials();
        fetch();
    }

}
