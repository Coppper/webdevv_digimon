package com.merck.webdevdbsetup;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
/**
 *
 * @author cooper
 */
public class DatabaseFiller {

    private final String CONNECTION;
    private final String USER;
    private final String PASSWORD;

    /**
     * Default Constructor
     */
    public DatabaseFiller() {
        this.CONNECTION = "";
        this.USER = "";
        this.PASSWORD = "";
    }

    /**
     * Parameterized Constructor
     *
     * @param connection
     * @param user
     * @param password
     */
    public DatabaseFiller(String connection, String user, String password) {
        this.CONNECTION = connection;
        this.USER = user;
        this.PASSWORD = password;
    }

    /**
     * This method handles the SQL query for inserting
     * a card into our database
     * @param input
     * @throws SQLException 
     */
    public void insertCard(ArrayList<String> input) throws SQLException {
        System.out.println(input);
        String cardQuery = "INSERT INTO Cards"
                + " (cardId, setId, cardName, cardType, subType, typing, evolvesTo, evolvesFrom, rarity, flavorText, nationaldex, artist, cardApiLink)"
                + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

        try (Connection connection = DriverManager.getConnection(CONNECTION, USER, PASSWORD);
                PreparedStatement cardStatement = connection.prepareStatement(cardQuery);) {

            fillCardTable(cardStatement, input);
            cardStatement.executeUpdate();
            System.out.println("Card added");
        }
    }

    /**
     * Helper method to prep the SQL query 
     * @param cardStatement
     * @param input 
     */
    private void fillCardTable(PreparedStatement cardStatement, ArrayList<String> input) {
        try {
            for (int i = 0; i < input.size(); i++) {
                cardStatement.setString(i + 1, input.get(i));
            }

        } catch (SQLException sqle) {
            System.out.println("can't set card statement");
        }
    }

    /**
     * This method handles the SQL Query for inserting
     * a set into our database
     * @param input
     * @throws SQLException 
     */
    public void insertSet(ArrayList<String> input) throws SQLException {
        String setQuery = "INSERT INTO CardSets"
                + " (setId, setName, series, total, releaseDate, setApiLink)"
                + " VALUES (?, ?, ?, ?, ?, ?);";

        try (Connection connection = DriverManager.getConnection(CONNECTION, USER, PASSWORD);
                PreparedStatement setStatement = connection.prepareStatement(setQuery);) {

            fillSetTable(setStatement, input);
            setStatement.executeUpdate();
            System.out.println("set added");
        }

    }

    /**
     * Helper method to prep SQL query
     * @param setStatement
     * @param input 
     */
    private void fillSetTable(PreparedStatement setStatement, ArrayList<String> input) {
        try {
            for (int i = 0; i < input.size(); i++) {
                setStatement.setString(i + 1, input.get(i));
            }
        } catch (SQLException sqle) {
            System.out.println("can't set Set statement");
        }
    }
}
