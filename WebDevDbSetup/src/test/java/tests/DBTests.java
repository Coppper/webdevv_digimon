package tests;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.*;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author coope
 */
public class DBTests {

    // FILL IN WITH GIVEN CREDENTIALS
    private String CONNECTION = "";
    private String USER = "";
    private String PASSWORD = "";

    /**
     * This test makes sure we get a proper connection to our database
     */
    @Test
    public void testConnection() {
        try {
            Connection connection = DriverManager.getConnection(CONNECTION, USER, PASSWORD);
            if (connection != null) {
                assertNotNull(connection);
            } else {
                fail("Connection is null");
            }
        } catch (SQLException sqle) {
            fail("SQLException thrown");
        }
    }

    /**
     * This method tests to see that we're able to select a specific card
     */
    @Test
    public void testSelectCard() {
        String query = "SELECT * FROM Cards WHERE cardId = 'base1-1'";
        try {
            try (Connection connection = DriverManager.getConnection(CONNECTION, USER, PASSWORD);
                    PreparedStatement pStatement = connection.prepareStatement(query);) {

                try (ResultSet rSet = pStatement.executeQuery()) {
                    if (rSet.next()) {
                        String cardname = rSet.getString("cardname");
                        assertEquals("Alakazam", cardname);
                    } else {
                        fail("result set came back empty");
                    }
                }
            }
        } catch (SQLException sqle) {
            fail("SQLException was thrown");
        }
    }

    /**
     * This method tests that we can successfully retrieve users
     */
    @Test
    public void testSelectUser() {
        String query = "SELECT * FROM Users WHERE username = 'test'";
        try {
            try (Connection connection = DriverManager.getConnection(CONNECTION, USER, PASSWORD);
                    PreparedStatement pStatement = connection.prepareStatement(query);) {

                try (ResultSet rSet = pStatement.executeQuery()) {
                    if (rSet.next()) {
                        String username = rSet.getString("username");
                        assertEquals("test", username);
                    } else {
                        fail("result set came back empty");
                    }
                }
            }
        } catch (SQLException sqle) {
            fail("SQLException was thrown");
        }
    }

    /**
     * This method tests whether we can successfully retrieve a set from the DB
     */
    @Test
    public void testSelectSet() {
        String query = "SELECT * FROM cardSets WHERE setId = 'base3'";
        try {
            try (Connection connection = DriverManager.getConnection(CONNECTION, USER, PASSWORD);
                    PreparedStatement pStatement = connection.prepareStatement(query);) {

                try (ResultSet rSet = pStatement.executeQuery()) {
                    if (rSet.next()) {
                        String setName = rSet.getString("setName");
                        assertEquals("Fossil", setName);
                    } else {
                        fail("the wrong set was returned");
                    }
                }
            }
        } catch (SQLException sqle) {
            fail("SQLException was thrown");
        }
    }

    /**
     * This method tests that cards are properly inserted into a collection
     */
    @Test
    public void testCardsInCollection() {
        String query = "SELECT * FROM collectionbridge WHERE collectionId = '1'";
        ArrayList<String> cards = new ArrayList<String>();
        try {
            try (Connection connection = DriverManager.getConnection(CONNECTION, USER, PASSWORD);
                    PreparedStatement pStatement = connection.prepareStatement(query);) {

                try (ResultSet rSet = pStatement.executeQuery()) {
                    while (rSet.next()) {
                        cards.add(rSet.getString("cardId"));
                    }
                    assertNotNull(cards);
                }
            }
        } catch (SQLException sqle) {
            fail("SQLException was thrown");
        }
    }

}
