--Drop tables 
DROP TABLE Users CASCADE ;
DROP TABLE CardSets CASCADE;
DROP TABLE Cards CASCADE;
DROP TABLE Collections CASCADE;
DROP TABLE CollectionBridge CASCADE;
DROP TABLE Comments CASCADE;
DROP TABLE Follow CASCADE;
DROP TABLE Ratings CASCADE;

CREATE TABLE CardSets (
    setId VARCHAR(10) NOT NULL,
    setName VARCHAR(100) NOT NULL,
    series VARCHAR(100) NOT NULL,
    total VARCHAR(100) NOT NULL,
    releaseDate VARCHAR(100) NOT NULL,
    setApiLink VARCHAR(200),
    CONSTRAINT setId PRIMARY KEY (setId)
);

CREATE TABLE Cards (
    cardId VARCHAR(30) NOT NULL,
    setId VARCHAR(30) NOT NULL,
    cardName VARCHAR(60) NOT NULL,
    cardType VARCHAR(60) NOT NULL,-- Pokemon, Energy or Trainer
    subType VARCHAR(60) NOT NULL, -- Basic, EX, etc
    typing VARCHAR(100) NOT NULL, -- Attribute (water, fire, etc)
    evolvesTo VARCHAR(100) NOT NULL,
    evolvesFrom VARCHAR(100) NOT NULL,
    rarity VARCHAR(100) NOT NULL,
    flavorText VARCHAR(750) NOT NULL,
    nationaldex VARCHAR(100) NOT NULL,
    artist VARCHAR(150) NOT NULL,
    cardApiLink VARCHAR(150),
    CONSTRAINT cardId PRIMARY KEY (cardId),
    CONSTRAINT setId FOREIGN KEY (setId) REFERENCES CardSets(setId)
);

CREATE TABLE Users (
    userId SERIAL,
    bio VARCHAR(200),
    location VARCHAR(20),
    username VARCHAR(100) UNIQUE,
    profilepic VARCHAR(250),
    bannerpic VARCHAR(250),
    CONSTRAINT userId PRIMARY KEY (userId)
);

CREATE TABLE Collections (
    collectionId SERIAL,
    collectionName VARCHAR(50) NOT NULL,
    userId INT NOT NULL,
    ranking INT UNIQUE,
    CONSTRAINT collectionId PRIMARY KEY (collectionId),
    CONSTRAINT uniqueNames UNIQUE(collectionName, userId),
    CONSTRAINT userId FOREIGN KEY (userId) REFERENCES Users(userId) ON DELETE CASCADE
);

CREATE TABLE CollectionBridge (
    cardId VARCHAR(30) NOT NULL,
    collectionId INT NOT NULL,
    CONSTRAINT cardId FOREIGN KEY (cardId) REFERENCES Cards(cardId) ON DELETE CASCADE,
    CONSTRAINT collectionId FOREIGN KEY (collectionId) REFERENCES Collections(collectionId) ON DELETE CASCADE
);

CREATE TABLE Comments ( 
    commentId SERIAL,
    cardId VARCHAR(10) NOT NULL,
    userId INT NOT NULL,
    content VARCHAR(250),
    CONSTRAINT commentId PRIMARY KEY (commentId),
    CONSTRAINT cardId FOREIGN KEY (cardId) REFERENCES Cards(cardId) ON DELETE CASCADE
    CONSTRAINT userId FOREIGN KEY (userId) REFERENCES Users(userId) ON DELETE CASCADE
);

CREATE TABLE Comment(
    commentId SERIAL,
    cardId VARCHAR(30) NOT NULL,
    userId INT NOT NULL,
    content VARCHAR(250),
    CONSTRAINT userId FOREIGN KEY (userId) REFERENCES Users(userId) ON DELETE CASCADE,    
    CONSTRAINT cardId FOREIGN KEY (cardId) REFERENCES Cards(cardId) ON DELETE CASCADE
);

CREATE TABLE Follow (
    userId INT NOT NULL,
    followId INT NOT NULL,
    CONSTRAINT userId FOREIGN KEY (userId) REFERENCES Users(userId) ON DELETE CASCADE,    
    CONSTRAINT followId FOREIGN KEY (followId) REFERENCES Users(userId) ON DELETE CASCADE;
);

CREATE TABLE Ratings (
    cardId VARCHAR(30) NOT NULL,
    rating DECIMAL DEFAULT 0,
    CONSTRAINT cardId FOREIGN KEY (cardId) REFERENCES Cards(cardId)
);