const express = require("express");
const app = express.Router();
const cors = require("cors");
const bodyParser = require("body-parser");
const path = require('path');
// switch to individual controllers for each model?
const cardController = require("../controllers/card");
const commentController = require("../controllers/comment");
const collectionController = require("../controllers/collection");
const userController = require("../controllers/user");
const followController = require("../controllers/follow");
const ratingController = require("../controllers/rating");

const environment = process.env.NODE_ENV || "development";

// allows us to use different domain applications to interact (locally)
app.use(cors());
// gives us accss to request.body to get JSON data
app.use(express.json());

//Body parser
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({strict: false}));

//Serving built static files on the production server
if(environment === "production") {
  app.use(express.static(path.join(__dirname, '../../client/build')));
}


/**
 * @swagger
 * components:
 *   schemas:
 *     Card:
 *       type: object
 *       properties:
 *         cardid:
 *           type: string
 *           description: The card ID.
 *           example: base1-10
 *         setid:
 *           type: string
 *           description: The id of the set the card is part of.
 *           example: base1
 *         cardname:
 *           type: string
 *           description: The name of the Pokemon
 *           example: Mewtwo
 *         cardtype:
 *           type: string
 *           description: The type of the card
 *           example: Pokémon
 *         subtype:
 *           type: string
 *           description: The subtype of the card
 *           example: Basic
 *         typing:
 *           type: string
 *           description: The type of the card
 *           example: Psychic
 *         evolvesto:
 *           type: string
 *           description: The next evolution of the Pokemon
 *           example: No Evolution
 *         evolvesfrom:
 *           type: string
 *           description: The previous evolution of the Pokemon
 *           example: No Pre-Evolution
 *         rarity:
 *           type: string
 *           description: The rarity of the card
 *           example: Rare Holo
 *         flavortext:
 *           type: string
 *           description: The text at the bottom of the card
 *           example: A scientist created this Pokémon after years of horrific gene-splicing and DNA engineering experiments.
 *         nationaldex:
 *           type: string
 *           description: I have no clue what this is
 *           example: 150
 *         artist:
 *           type: string
 *           description: The artist who created the card artwork
 *           example: Ken Sugimori
 *         cardapilink:
 *           type: string
 *           description: The link to the image
 *           example: https://pokemonblobs.blob.core.windows.net/images/base1/10
 *     User:
 *       type: object
 *       properties:
 *         userid:
 *           type: integer
 *           description: The ID of the user
 *           example: 999
 *         bio:
 *           type: string
 *           description: The bio of the user
 *           example: This is a bio
 *         location:
 *           type: string
 *           description: The location of the user
 *           example: Montreal
 *         username:
 *           type: string
 *           description: The username of the user
 *           example: Xx_PokemonMaster_xX
 *         profilepic:
 *           type: string
 *           description: The link to the user's profile picture
 *         bannerpic:
 *           type: string
 *           description: The link to the user's banner picture
 *     Follow:
 *       type: object
 *       properties:
 *         userid:
 *           type: integer
 *           description: The ID of the user
 *           example: 999
 *         followid:
 *           type: string
 *           description: The bio of the followed user
 *           example: 856
 *     Collection:
 *       type: object
 *       properties:
 *         collectionid:
 *           type: integer
 *           description: The ID of the collection
 *           example: 999
 *         collectionname:
 *           type: string
 *           description: The name of the collection
 *           example: Good Collection
 *         userid:
 *           type: integer
 *           description: The id of the collection
 *           example: 78
 *         ranking:
 *           type: integer
 *           description: The position of the collection in the list
 *           example: 7
 */

/* Card endpoints */
/**
 * @swagger
 * /cards:
 *  get:
 *    summary: Retrieves the list of all cards in the database
 *    responses:
 *       200:
 *         description: A list of cards.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 cards:
 *                   type: array
 *                   items: 
 *                     $ref: '#/components/schemas/Card'            
 */
app.get('/cards', cardController.getAllCards);

/**
 * @swagger
 * /cards/{id}:
 *  get:
 *    summary: Retrieves a specific card from the database
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: ID of the card to retrieve
 *        schema:
 *          type: integer
 *          example: base1-3
 *    responses:
 *       200:
 *         description: A card
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 cards:
 *                   type: object
 *                   $ref: '#/components/schemas/Card'         
 */
app.get('/cards/:id', cardController.getCard);

/**
 * @swagger
 * /cards/page/{num}:
 *  get:
 *    summary: Returns 60 cards with a desired offset of 10 times the param given
 *    parameters:
 *      - in: path
 *        name: num
 *        required: true
 *        description: The current page
 *        schema:
 *          type: integer
 *          example: 4
 *    responses:
 *       200:
 *         description: A list of cards
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 card:
 *                   type: array
 *                   items:
 *                     $ref: '#/components/schemas/Card'            
 */
app.get('/cards/page/:num', cardController.paginateBrowseCards)

/**
 * @swagger
 * /cards/random/{count}:
 *  get:
 *    summary: Retrieves a list of random card from the database
 *    parameters:
 *      - in: path
 *        name: count
 *        required: true
 *        description: How many cards to retrieve
 *        schema:
 *          type: integer
 *          example: 10
 *    responses:
 *       200:
 *         description: A list of cards.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 cards:
 *                   type: array
 *                   items: 
 *                     $ref: '#/components/schemas/Card'            
 */
app.get('/cards/random/:count', cardController.getRandomCards);

/**
 * @swagger
 * /cards/search/params:
 *  get:
 *    summary: Returns all the cards that matches with the given tags (up to 3)
 *    responses:
 *       200:
 *         description: A list of cards.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 cards:
 *                   type: array
 *                   items: 
 *                     $ref: '#/components/schemas/Card'            
 */
app.get('/cards/search/params', cardController.searchCardByParams);

/**
 * @swagger
 * /cards/search/params:
 *  get:
 *    summary: Returns possible suggestions for search results
 *    responses:
 *       200:
 *         description: A list of suggestions.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 cards:
 *                   type: array
 *                   items: 
 *                     type: string
 *                     example: Pikachu           
 */
app.get('/cards/suggestion/:input',cardController.suggestionOfCards);

/**
 * @swagger
 * /cards/upload/{id}:
 *  post:
 *    summary: Adds a card image link to the database
 *    responses:
 *       200:
 *         description: The updated card
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 cards:
 *                   type: array
 *                   items: 
 *                     $ref: '#/components/schemas/Card'            
 */
app.post('/cards/upload/:id', cardController.addCardImage);

/**
 * @swagger
 * /collections/cards:
 *  post:
 *    summary: Adds a card to a collection
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: The collection id
 *        schema:
 *          type: integer
 *          example: 10     
 *      - in: params
 *        name: id
 *        required: true
 *        description: The card id
 *        schema:
 *          type: integer
 *          example: 10      
 */
app.post('collections/cards/:id', cardController.addCardToCollection);

/**
 * @swagger
 * /cards/{cardid}/rating:
 *  get:
 *    summary: Retrieves the ratings for a specific card
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: ID of the card to retrieve the ratings for
 *        schema:
 *          type: string
 *          example: base1-3
 *    responses:
 *       200:
 *         description: An array of ratings
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 type: object
 *                 properties:
 *                   rating:
 *                     type: string
 *                     example: 3      
 */
app.get('/cards/:cardid/rating', ratingController.getRating);

/**
 * @swagger
 * /cards/{cardid}/rating:
 *  post:
 *    summary: Sets the ratings for a specific card
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: ID of the card to set the ratings for
 *        schema:
 *          type: string
 *          example: base1-3  
 */
app.post('/cards/:cardid/rating', ratingController.postRating);
app.get('/cards/suggestion/:input',cardController.suggestionOfCards)





/* Comments endpoints */
/**
 * @swagger
 * /cards/{cardid}/comments:
 *  get:
 *    summary: Retrieves the comments posted on a specific card
 *    parameters:
 *      - in: path
 *        name: cardid
 *        required: true
 *        description: ID of the card to retrieve the comments for
 *        schema:
 *          type: integer
 *          example: base1-3
 *    responses:
 *       200:
 *         description: A card
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                  type: string
 *                  example: "This is a comment"       
 */
app.get('/cards/:cardid/comments/', cardController.getCommentsOnCard);

/**
 * @swagger
 * /cards/{cardid}/comment/{userid}:
 *  post:
 *    summary: Posts a comment on a card
 *    parameters:
 *      - in: path
 *        name: cardid
 *        required: true
 *        description: ID of the card to comments on
 *        schema:
 *          type: string
 *          example: base1-3
 *      - in: path
 *        name: userid
 *        required: true
 *        description: user id of the commenter
 *        schema:
 *          type: integer
 *          example: 53
 *      - in: body
 *        name: content
 *        required: true
 *        description: The body of the comment
 *        schema:
 *          type: string
 *          example: This is a comment    
 */
app.post('/cards/:cardid/comment/:userid', commentController.postComment);

/**
 * @swagger
 * /cards/{cardid}/comment/{userid}:
 *  get:
 *    summary: Removes a specific comment
 *    parameters:
 *      - in: path
 *        name: cardid
 *        required: true
 *        description: ID of the card to comments on
 *        schema:
 *          type: string
 *          example: base1-3
 *      - in: path
 *        name: userid
 *        required: true
 *        description: user id of the commenter
 *        schema:
 *          type: integer
 *          example: 53
 *    responses:
 *       200:
 *         description: A card
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                  type: string
 *                  example: "This is a comment"       
 */
app.delete('/cards/:cardid/comment/:userid', commentController.deleteComment);




/* Collections endpoints */
/**
 * @swagger
 * /collections/{userid}:
 *  get:
 *    summary: Retrieves every collection a user has
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: ID of the user
 *        schema:
 *          type: integer
 *          example: 999
 *    responses:
 *       200:
 *         description: A list of collections
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 collections:
 *                   type: object
 *                   $ref: '#/components/schemas/Collection'        
 */
app.get('/collections/:userid', collectionController.getUsersCollections);

/**
 * @swagger
 * /collections/favorites/{userid}:
 *  get:
 *    summary: Retrieves the list of all cards in the user's favorite collection
 *    responses:
 *       200:
 *         description: A list of cards.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 cards:
 *                   type: array
 *                   items: 
 *                     $ref: '#/components/schemas/Card'            
 */
app.get('/collections/favorites/:userid', collectionController.getFavoriteCollectionCards);

/**
 * @swagger
 * /collection/{collectionid}:
 *  get:
 *    summary: Retrieves a single collection by its id
 *    parameters:
 *      - in: path
 *        name: collectionid
 *        required: true
 *        description: ID of the collection
 *        schema:
 *          type: integer
 *          example: 999
 *    responses:
 *       200:
 *         description: A collections
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               $ref: '#/components/schemas/Collection'        
 */
app.get('/collection/:collectionid', collectionController.getCollectionById);

/**
 * @swagger
 * /collection/{collectionid}:
 *  delete:
 *    summary: Retrieves a card from a collection
 *    parameters:
 *      - in: path
 *        name: collectionid
 *        required: true
 *        description: ID of the collection
 *        schema:
 *          type: integer
 *          example: 999
 *      - in: body
 *        name: cardid
 *        required: true
 *        description: ID of the card
 *        schema:
 *          type: integer
 *          example: 999     
 */
app.delete('/collection/:collectionid', collectionController.deleteCardFromCollection);

/**
 * @swagger
 * /collection:
 *  post:
 *    summary: Adds a new collection
 *    parameters:
 *      - in: path
 *        name: collectionname
 *        required: true
 *        description: Name of the collection
 *        schema:
 *          type: string
 *          example: bruh 
 */
app.post('/collections/', collectionController.addCollection);

/**
 * @swagger
 * /collections/cards/{id}:
 *  post:
 *    summary: Adds a card from to collection
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: ID of the card
 *        schema:
 *          type: integer
 *          example: 999
 *      - in: body
 *        name: collectionid
 *        required: true
 *        description: ID of the collection
 *        schema:
 *          type: integer
 *          example: 999     
 */
app.post('/collections/cards/:id', cardController.addCardToCollection);

/**
 * @swagger
 * /collections/rank/{id}:
 *  put:
 *    summary: Updates the rank of a collection
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: ID of the collection
 *        schema:
 *          type: integer
 *          example: 999
 *      - in: body
 *        name: ranking
 *        required: true
 *        description: new rank of the card
 *        schema:
 *          type: integer
 *          example: 999     
 */
app.put('/collections/rank/:id', collectionController.updateCollectionRank);




/* User endpoints */
/**
 * @swagger
 * /users/{userid}:
 *  get:
 *    summary: Retrieves a specific user given the id
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: ID of the user
 *        schema:
 *          type: integer
 *          example: 999
 *    responses:
 *       200:
 *         description: A user
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               $ref: '#/components/schemas/User'         
 */
app.get('/users/:userid', userController.getUserById);

/**
 * @swagger
 * /users/username/{username}:
 *  get:
 *    summary: Retrieves a specific user given the username
 *    parameters:
 *      - in: path
 *        name: username
 *        required: true
 *        description: username of the user
 *        schema:
 *          type: string
 *          example: testuser
 *    responses:
 *       200:
 *         description: A user
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               $ref: '#/components/schemas/User'         
 */
app.get('/users/username/:username', userController.getUserByName);

/**
 * @swagger
 * /users/{userid}:
 *  post:
 *    summary: Updates a user profile   
 *    parameters:
 *      - in: body
 *        name: location
 *        description: New location of the user
 *        schema:
 *          type: string
 *          example: Montreal     
 *      - in: body
 *        name: bio
 *        description: New bio of the user
 *        schema:
 *          type: string
 *          example: This is a bio     
 */
app.post('/users/:userid', userController.editUser);

/**
 * @swagger
 * /users:
 *  post:
 *    summary: Creates a new user profile   
 *    parameters:
 *      - in: body
 *        name: location
 *        description: location of the user
 *        schema:
 *          type: string
 *          example: Montreal     
 *      - in: body
 *        name: bio
 *        description: bio of the user
 *        schema:
 *          type: string
 *          example: This is a bio     
 *      - in: body
 *        name: username
 *        description: Username of the user
 *        schema:
 *          type: string
 *          example: This is a bio     
 */
app.post('/users', userController.addUser);




/* Friends endpoints */
/**
 * @swagger
 * /follow/{userid}:
 *  get:
 *    summary: Retrieves everyone a user follows
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: ID of the user
 *        schema:
 *          type: integer
 *          example: 999
 *    responses:
 *       200:
 *         description: A list of follows
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 cards:
 *                   type: object
 *                   $ref: '#/components/schemas/Follow'        
 */
app.get('/follow/:userid', followController.getUserFollows);

/**
 * @swagger
 * /followers/{userid}:
 *  get:
 *    summary: Retrieves everyone who follows a user
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: ID of the user
 *        schema:
 *          type: integer
 *          example: 999
 *    responses:
 *       200:
 *         description: A list of follows
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 cards:
 *                   type: object
 *                   $ref: '#/components/schemas/Follow'        
 */
app.get('/followers/:followid', followController.getUserFollowers);

/**
 * @swagger
 * /follow/{userid}/{followid}:
 *  get:
 *    summary: Check if someone is in a user's follow list
 *    parameters:
 *      - in: path
 *        name: userid
 *        required: true
 *        description: ID of the user
 *        schema:
 *          type: integer
 *          example: 999
 *      - in: path
 *        name: followid
 *        required: true
 *        description: ID of the user to check for
 *        schema:
 *          type: integer
 *          example: 999
 *    responses:
 *       200:
 *         description: A list of follows
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 cards:
 *                   type: object
 *                   $ref: '#/components/schemas/Follow'        
 */
app.get('/follow/:userid/:followid', followController.getUserSpecificFollow)

/**
 * @swagger
 * /follow/{userid}/{followid}:
 *  post:
 *    summary: Adds a user to another's follow list
 *    parameters:
 *      - in: path
 *        name: userid
 *        required: true
 *        description: ID of the user
 *        schema:
 *          type: integer
 *          example: 999
 *      - in: path
 *        name: followid
 *        required: true
 *        description: ID of the user to add
 *        schema:
 *          type: integer
 *          example: 999    
 */
app.post('/follow/:userid/:followid', followController.addUserFollow);

/**
 * @swagger
 * /follow/{userid}/{followid}:
 *  delete:
 *    summary: Removes a user from another's follow list
 *    parameters:
 *      - in: path
 *        name: userid
 *        required: true
 *        description: ID of the user
 *        schema:
 *          type: integer
 *          example: 999
 *      - in: path
 *        name: followid
 *        required: true
 *        description: ID of the user to remove
 *        schema:
 *          type: integer
 *          example: 999    
 */
app.delete('/follow/:userid/:followid', followController.deleteUserFollow);


// This route will handle all the requests that are not handled by any other route
app.get('*', (req, res) => {
  res.status(404).send('Page not found');
});

module.exports = app;
