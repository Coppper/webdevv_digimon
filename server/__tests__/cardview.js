const app = require("../app.js");
const supertest = require("supertest");
const request = supertest(app);
const dbConnection = require("../database/models/index");
const models = require("../database/models");

/**
 * /card/:cardid
 * Tests detail card view for the following functionality for 'end to end' testing
 * - Rate a card
 * - Comment on a card
 * - Delete a comment on a card
 */

const cardid = 'base1-11';

// TRUNCATE all the tables needed so that they are empty
beforeAll(async () => {
    // Truncate user table
    await models.user.destroy({
        truncate: true,
        restartIdentity: true // reset the auto increment to start at 1
    });
    // Truncate rating table
    await models.rating.destroy({
        truncate: true,
        restartIdentity: true // reset the auto increment to start at 1
    });
    // Truncate comment table
    await models.comment.destroy({
        truncate: true,
        restartIdentity: true // reset the auto increment to start at 1
    });
});

describe('Card View', () => {
    // any further functionality we need a user in the system
    // POST user /api/users
    it('Tests adding a username to the database', async () => {
        const username = { username: "pokemonmaster" };
        // this adds the user to the database (just their username)
        const response = await request.post(`/api/users`).send(username);
        expect(response.status).toBe(201);
    })
    // GET card/:cardid
    it('Tests the get card by id endpoint', async () => {
        const response = await request.get(`/api/cards/${cardid}`);
        // the expected result could contain the cardapilink as null since we're connecting to the test db
        const expected = `{"card":{"cardid":"base1-11","setid":"base1","cardname":"Nidoking","cardtype":"Pokémon","subtype":"Stage 2","typing":"Grass","evolvesto":"No Evolution","evolvesfrom":"Nidorino","rarity":"Rare Holo","flavortext":"Uses its powerful tail in battle to smash, constrict, then break its prey's bones.","nationaldex":"34","artist":"Ken Sugimori","cardapilink":null,"cardset":{"setid":"base1","setname":"Base","series":"Base","total":"102","releasedate":"1999/01/09","setapilink":null}}}`
        expect(response.text).toBe(expected);
        expect(response.status).toBe(200);
    })
    // GET cards/:cardid/rating
    it('Tests getting the current ratings', async () => {
        const response = await request.get(`/api/cards/${cardid}/rating`);
        const expected = "[]"; // empty array since nothing in the db yet
        expect(response.text).toBe(expected);
        expect(response.status).toBe(200);
    })
    // POST cards/:cardid/rating/1?userAuth=1
    it('Tests adding a new rating', async () => {
        const rating = 4.24;
        const response = await request.post(`/api/cards/${cardid}/rating?userAuth=true&rating=${rating}`);
        expect(response.status).toBe(201);
    })
    // POST comment cards/:cardid/comment/:userid?userAuth=true
    it('Tests adding a comment to card', async () => {
        const commentJson = {
            content: "a really cool thing to say",
        };
        const userid = 1;
        const authenticated = true;
        let addCommentUrl = `/api/cards/${cardid}/comment/${userid}?userAuth=${authenticated}`;
        const response = await request.post(addCommentUrl).send(commentJson);
        expect(response.status).toBe(201);
    })
    // GET comment /api/cards/:cardid/comments
    it('Tests getting comments on a card', async () => {
        const response = await request.get(`/api/cards/${cardid}/comments`);
        expect(response.status).toBe(200);
        expect(response.text).toMatch(/a really cool thing to say/);
    })
    // DELETE comment cards/:cardid/comment/:userid?userAuth=true&delete=true&commentid=x
    it('Tests deleting comment on a card', async () => {
        const userid = 1;
        const authenticated = true;
        const commentid = 1;
        let deleteCommentUrl = `/api/cards/${cardid}/comment/${userid}?userAuth=${authenticated}&delete=true&commentid=${commentid}`;
        const response = await request.delete(deleteCommentUrl);
        expect(response.status).toBe(200);
    })
});

afterAll(async () => {
    // Closing the DB connection allows Jest to exit successfully.
    await dbConnection.sequelize.close();
});