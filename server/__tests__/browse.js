const app = require("../app.js");
const supertest = require("supertest");
const request = supertest(app);
const dbConnection = require("../database/models/index");

/**
 * /browse
 * Tests the browse view for the following functionality for 'end to end' testing
 * - Get all cards
 * - Get random number of cards
 * - Get cards by page number
 * - Get card(s) by search params
 * - Add card image
 */

// GET /cards
// needed for any other tests
beforeAll(async () => {
    const response = await request.get('/api/cards');
    const cards = response.body.cards[0];
    const numOfCards = response.body.cards.length
    expect(response.status).toBe(200);
    expect(numOfCards).toBe(14410) // the total number of cards
    expect(cards).toHaveProperty('cardid', 'setid', 'cardname', 'cardtype', 'subtype', 'typing', 'evolvesto', 'evolvesfrom', 'rarity', 'flavortext', 'nationaldex', 'artist', 'cardapilink');
});

describe('Browse View', () => {
    // GET /cards/page/:num
    it('Tests the endpoint for pagination', async () => {
        const maxCardsPerPage = 30;
        const pageNum = 1; // first page
        let response = await request.get(`/api/cards/page/${pageNum}`);
        // page 1 should have 60 cards
        expect(response.body.paginate.length).toBe(maxCardsPerPage);
        expect(response.status).toBe(200);
        // last page should have 14410 % 60 cards
        const lastPage = Math.trunc(14410 / maxCardsPerPage);
        response = await request.get(`/api/cards/page/${lastPage}`);
        expect(response.body.paginate.length).toBe(14410 % 60);
        expect(response.status).toBe(200);
    });

    // GET /cards/random/:count
    it('Tests getting random assortment of number of cards', async () => {
        const count = 36;
        const response = await request.get(`/api/cards/random/${count}`);
        expect(response.body.cards.length).toBe(count);
        expect(response.status).toBe(200);
    });

    // GET /cards/search/params
    it('Tests search cards by params', async () => {
        const tag0 = 'pika'
        const tag1 = 'rainbow'
        const response = await request.get(`/api/cards/search/params/?tag0=${tag0}&tag1=${tag1}`);
        for (const card of response.body.paginate) {
            // use jest regex to match strings
            expect(card.cardname).toEqual(expect.stringMatching(/[Pp]ika|[Rr]ainbow/));
        }
        expect(response.status).toBe(200);
        const tag3 = 'mcdonalds';
        const response2 = await request.get(`/api/cards/search/params/?tag0=${tag3}`);
        // no results for mcdonalds
        expect(response2.body.paginate.length).toBe(0);
    });

    // POST /cards/upload/:id
    it('Tests adding card image (cardapilink) link', async () => {
        const setid = "base1";
        const cardid = "99";
        const uploadJson = { cardapilink: `https://pokemonblobs.blob.core.windows.net/images/${setid}/${cardid}` };
        const response = await request.post(`/api/cards/upload/${setid}-${cardid}`).send(uploadJson);
        expect(response.status).toBe(201);
    });
});

afterAll(async () => {
    // Closing the DB connection allows Jest to exit successfully.
    await dbConnection.sequelize.close();
});
