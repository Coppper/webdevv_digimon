const app = require("../app.js");
const supertest = require("supertest");
const request = supertest(app);
const dbConnection = require("../database/models/index");
const models = require("../database/models");

/**
 * /collections
 * Tests the collection + add card to collection view for the following functionality for 'end to end' testing
 * - Add a new collection
 * - Add cards to a collection
 * - Get owner (user) of collection
 * - Get collection by collection id
 * - Delete card in a collection
 */

// initialize some variables to use for tests
const userid = 1;
const collectionid = 1;
const username = "pokemonmaster"
const authenticated = true;
const cards = ['base1-1', 'bw6-108', 'ecard3-75', 'sm10-61', 'swsh45sv-SV066'];

// TRUNCATE all the tables needed so that they are empty
beforeAll(async () => {
    // Truncate user table
    await models.user.destroy({
        truncate: true,
        restartIdentity: true // reset the auto increment to start at 1
    });
    // Truncate collection table
    await models.collections.destroy({
        truncate: true,
        restartIdentity: true // reset the auto increment to start at 1
    });
    // Truncate collectionbridge table
    await models.collectionbridge.destroy({
        truncate: true,
        restartIdentity: true // reset the auto increment to start at 1
    });
    // any further functionality we need a user in the system
    const newuser = { username: username };
    const response = await request.post(`/api/users`).send(newuser);
});

describe('Collections View', () => {
    // POST /api/collections?userAuth=:authenticated&user=:userid
    it('Add a new collection', async () => {
        const collectionJson = {
            collectionname: "my very first collection!!",
        };
        const response = await request.post(`/api/collections?userAuth=${authenticated}&user=${username}`).send(collectionJson);
        expect(response.text).toMatch(/my very first collection!!/);
        expect(response.status).toBe(201);
    })
    // POST /api/collections?userAuth=:authenticated&user=:userid
    it('Add cards to a collection', async () => {
        for (const card of cards) {
            const response = await request.post(`/api/collections/cards/${card}?userAuth=${authenticated}&collectionid=${collectionid}`).send();
            expect(response.status).toBe(201);
        }
    })
    // GET '/api/users/:userid)
    it('Get owner of collection', async () => {
        const response = await request.get(`/api/users/${userid}`);
        expect(response.body.user.username).toBe(username);
        expect(response.status).toBe(200);
    })
    // GET /api/collection/:collectionid
    it('Get cards in collection by id', async () => {
        const response = await request.get(`/api/collection/${collectionid}`);
        const cardsResponse = response.body.collections[0].collectionbridges[0].cards;
        // check that all the cards are the ones we added
        let index = 0;
        for (const card of cardsResponse) {
            expect(card.cardid).toBe(cards[index]);
            index++;
        }
        expect(response.status).toBe(200);
    })
    // DELETE card /api/collection/:collectionid?userAuth=:auth&delete=true&cardid=:cardid
    it('Delete a card in collection', async () => {
        let deleteCardUrl = `/api/collection/${collectionid}?userAuth=${authenticated}&delete=true&cardid=${cards[0]}`;
        const response = await request.delete(deleteCardUrl);
        expect(response.status).toBe(201);
    })
});

afterAll(async () => {
    // Closing the DB connection allows Jest to exit successfully.
    await dbConnection.sequelize.close();
});