const app = require("../app.js");
const supertest = require("supertest");
const request = supertest(app);
const dbConnection = require("../database/models/index");
const models = require("../database/models");

/**
 * /lists & homepage
 * Tests the listview for the following functionality for 'end to end' testing
 * - Get all collections from user
 * - Get 'favourites' collection from user
 * - Update collection rank
 */

// initialize some variables to use for tests
const userid = 1;
const username = "pokemonmaster"
const authenticated = true;

// TRUNCATE all the tables needed so that they are empty
beforeAll(async () => {
    // Truncate user table
    await models.user.destroy({
        truncate: true,
        restartIdentity: true // reset the auto increment to start at 1
    });
    // Truncate collection table
    await models.collections.destroy({
        truncate: true,
        restartIdentity: true // reset the auto increment to start at 1
    });
    // Truncate collectionbridge table
    await models.collectionbridge.destroy({
        truncate: true,
        restartIdentity: true // reset the auto increment to start at 1
    });
    // any further functionality we need a user in the system
    const newuser = { username: username };
    const response = await request.post(`/api/users`).send(newuser);
});

describe('Collections View', () => {
    // POST /api/collections?userAuth=:authenticated&user=:userid
    const collections = [
        { collectionname: "Should I sell these?" },
        { collectionname: "Fav Y2K Cards" },
        { collectionname: "PoKeBoLl Or PoKeMoN" },
        { collectionname: "First cards I ever collected" }
    ]
    it('Add new (empty) collections', async () => {
        for (const collection of collections) {
            const response = await request.post(`/api/collections?userAuth=${authenticated}&user=${username}`).send(collection);
            expect(response.status).toBe(201);
        }
    })
    // GET /collections/favorites/:userid
    it('Get favourite list from user', async () => {
        const response = await request.get(`/api/collections/favorites/${userid}`);
        // user will not have any favourites
        expect(response.body.cards).toStrictEqual([]);
        expect(response.status).toBe(200);
    })
    // GET /collections/:userid
    it('Get collections from user', async () => {
        const response = await request.get(`/api/collections/${userid}`);
        let index = 0;
        for (const collectionJson of response.body.collections) {
            expect(collectionJson.collectionname).toBe(collections[index].collectionname);
            index++;
        }
        expect(response.status).toBe(200);
    })
    // PUT /collections/rank/:id
    it('Updates the rank for a collection', async () => {
        const collectionid = 3;
        const rankJson = { rank: 1 }
        const response = await request.put(`/api/collections/rank/${collectionid}`).send(rankJson);
    })
});

afterAll(async () => {
    // Closing the DB connection allows Jest to exit successfully.
    await dbConnection.sequelize.close();
});
