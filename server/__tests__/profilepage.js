const app = require("../app.js");
const supertest = require("supertest");
const request = supertest(app);
const dbConnection = require("../database/models/index");
const models = require("../database/models");

/**
 * /profile
 * Tests the profile page for the following functionality for 'end to end' testing
 * - Get users followers
 * - Get users that user follows
 * - Get specific user that a user follows
 * - Add follow for user
 * - Delete follow for user
 */


// initialize some variables to use for tests
const userid = 1;
const username = "pokemonmaster"
const followers = [{ username: "SideShowBob" }, { username: "Ash" }, { username: "KoolAidKid" }, { username: "IPALover" }]
const friends = [{ username: "HomerSimpson" }, { username: "SailorMoon" }, { username: "Y2KBug" }, { username: "KFC" }]

// TRUNCATE all the tables needed so that they are empty
beforeAll(async () => {
    // Truncate user table
    await models.user.destroy({
        truncate: true,
        restartIdentity: true // reset the auto increment to start at 1
    });
    await models.follow.destroy({
        truncate: true,
    });
    // any further functionality we need a user in the system
    const newuser = { username: username };
    const response = await request.post(`/api/users`).send(newuser);
    // we also need some followers, lets add those followers in the db
    for (const friend of followers) {
        let friendResponse = await request.post(`/api/users`).send(friend);
    }
});

describe('Profile View', () => {
    // GET /api/collections?userAuth=:authenticated&user=:userid
    it('Get the user info by username', async () => {
        const response = await request.get(`/api/users/username/${username}`);
        const expected = {
            "user": {
                "userid": userid,
                "bio": null,
                "location": null,
                "username": username,
                "profilepic": null,
                "bannerpic": null
            }
        }
        expect(response.body).toStrictEqual(expected);
        expect(response.status).toBe(200);
    })
    // POST /follow/:userid/:followid
    it('Add followers for user', async () => {
        // have each user follow me (here userid/followid are reversed)
        for (let followid = 2; followid < followers.length + 2; followid++) {
            const response = await request.post(`/api/follow/${followid}/${userid}`);
            expect(response.status).toBe(201);
        }
    })
    // GET /api/followers/:userid
    it('Get the followers for a given user', async () => {
        // get all the users that follow userid = 1
        const response = await request.get(`/api/followers/${userid}`);
        for (const follower of response.body.follow) {
            expect(follower.followid).toBe(userid)
        }
    })
    // POST /follow/:userid/:followid
    it('Add users to follow', async () => {
        const offset = 6;
        // follow each user in 'friends'
        for (let friendid = 6; friendid < followers.length + offset; friendid++) {
            const response = await request.post(`/api/follow/${userid}/${friendid}`);
            expect(response.status).toBe(201);
        }
    })
    // GET /follow/:userid
    it('Get the user(s) that a given user follows', async () => {
        const response = await request.get(`/api/follow/${userid}`);
        // we do not know their userid to compare, just check response is ok
        expect(response.status).toBe(200);
    })
    // GET /follow/:userid/:followid
    it('Get a specific user the user is following', async () => {
        const response = await request.get(`/api/follow/${userid}/6`);
        expect(response.status).toBe(200);
    })
    // DELETE /follow/:userid/:followid
    it('Delete a specific user the user is following', async () => {
        const offset = 2;
        // remove all the users userid follows
        for (let followid = 6; followid < followers.length + offset; followid++) {
            const response = await request.delete(`/follow/${userid}/${followid}`);
            expect(response.status).toBe(200);
        }
    })
    // POST /users/:userid
    it('Edit users profile infomation', async () => {
        const userUserInfo = {
            location: "Pokemon Stadium",
            bio: "protagonist of the Pokémon anime and certain manga series"
        };
        const response = await request.post(`/api/users/${userid}`).send(userUserInfo);
        expect(response.status).toBe(200);
    })
});

afterAll(async () => {
    // Closing the DB connection allows Jest to exit successfully.
    await dbConnection.sequelize.close();
});