require('dotenv').config();
const express = require("express");
const app = express();
const api = require("./routes/api.js");
const path = require("path")
const swaggerJSDoc = require("swagger-jsdoc");
const swaggerUI = require("swagger-ui-express");

const swaggerDefinition = {
  openapi: '3.0.0',
  info: {
    title: 'Express API for PokemonDB',
    version: '1.0.0',
    description:
      'This is a REST API application made with Express. It retrieves data from a database of Pokémon Cards',
  },
  servers: [
    {
      url: 'http://localhost:3001',
      description: 'Development server',
    },
    {
      url: 'https://poke-collection-staging.herokuapp.com/',
      description: 'Production server',
    },
  ],
};

const options = {
  swaggerDefinition,
  apis: ['./routes/*.js']
};

const swaggerSpec = swaggerJSDoc(options);

// Only parse query parameters into strings, not objects
app.set('query parser', 'simple');
// anything in the URL path / uses the Router api
app.use("/api", api);
app.use('/docs', swaggerUI.serve, swaggerUI.setup(swaggerSpec));

app.use(express.static(path.resolve(__dirname, '../client/build')));

// Redirect to client content when inputting react router URL
app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../client/build', 'index.html'));
  });

module.exports = app;