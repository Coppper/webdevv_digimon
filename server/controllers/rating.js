const models = require("../database/models");

/**
 * Gets the rating on a card
 * @param {*} req request from client
 * @param {*} res response from server
 * @returns the rating
 */
const getRating = async (req, res) => {
    try {
        const cardid = req.params.cardid;
        if (cardid) {
            const rating = await models.rating.findAll({
                attributes: ['rating'],
                where: {
                    cardid: cardid
                }
            });
            return res.status(200).json(rating);
        }
        else {
            return res.status(204).send("No ratings for this card");
        }
    } catch (error) {
        return res.status(500).send(error.message);
    }
};

const postRating = async (req, res) => {
    // add rating on a card
    // /cards/:cardid/rating?userAuth=keycloak&rating=x
    if (req.query.userAuth === 'true') { // is user logged in
        try {
            const rating = await models.rating.create({
                rating: req.query.rating,
                cardid: req.params.cardid,
            });
            return res.status(201).send();
        } catch (error) {
            return res.status(500).send(error.message);
        }
    }
};

module.exports = {
    getRating,
    postRating
};