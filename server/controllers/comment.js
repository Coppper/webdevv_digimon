const models = require("../database/models");

const postComment = async (req, res) => {
    // add comment on post
    // cards/:cardid/comment/:userid?userAuth=keycloak.authenticated
    if (req.query.userAuth === 'true') { // is user logged in
        try {
            const comment = await models.comment.create({
                userid: req.params.userid,
                cardid: req.params.cardid,
                content: req.body.content
            });
            return res.status(201).send();
        } catch (error) {
            return res.status(500).send(error.message);
        }
    }
};

const deleteComment = async (req, res) => {
    // cards/cardid/comment/userid?userAuth=keycloak.authenticated&delete=true&commentid=x
    if (req.query.userAuth === 'true' && req.query.delete) {
        try {
            const comment = await models.comment.destroy({
                where: {
                    commentid: req.query.commentid,
                    userid: req.params.userid,
                }
            });
            return res.status(200).send();
        } catch (error) {
            return res.status(500).send(error.message);
        }
    }
};


module.exports = {
    postComment,
    deleteComment
};