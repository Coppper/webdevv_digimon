const { restart } = require("nodemon");
const models = require("../database/models");
const { param } = require("../routes/api");
const Sequelize = require('sequelize');
const { sequelize } = require("../database/models");
const Op = Sequelize.Op;


const sanitizeTag = (tag) => {
  if(typeof tag[1] === "string")return tempValue = tag[1].normalize("NFKC")
    return tag[1][0].normalize("NFKC") 
}

/**
 * Gets all the cards from the database
 * @param {*} req request from client
 * @param {*} res response from server
 * @returns all the cards in JSON format (if found, otherwise 500 response code)
 */
const getAllCards = async (req, res) => {
  try {
    const cards = await models.card.findAll({});
    if(cards){
      return res.status(200).json({ cards });
    }
    return res.status(404).send("Unable to retrieve data for all cards");
  } catch (error) {
    return res.status(500).send(error.message);
  }
};

/**
 * Gets a single card from the database
 * @param {*} req request from client
 * @param {*} res response from server
 * @returns the card in JSON format (if found, otherwise 500 response code)
 */
const getCard = async (req, res) => {
  try {
    const cardId = req.params.id;
    const card = await models.card.findByPk(cardId,{
      include: {
        model: models.cardsets,
        foreignKey: 'setid',
        require: true
      }})
;
    if (card) {
      return res.status(200).json({ card });
    }
    return res.status(404).send("Card with the specified ID does not exists");
  } catch (error) {
    return res.status(500).send(error.message);
  }
};

/**
 * Updates a single card with the azure db link for its image
 * @param {*} req request from client
 * @param {*} res response from server
 * @returns 
 */
const addCardImage = async (req, res) => {
  // going to localhost:3000 not 3001?
  const cardID = req.params.id;
  try {
    const image = await models.card.update({
      cardapilink: req.body.cardapilink,
    }, { where : { cardid : cardID}});
    return res.status(201).send(image);
  } catch (error) {
    return res.status(500).send(`Image already exists for this card`);
  }
};

const getRandomCards = async(req, res) => {
  try {
    const count = req.params.count;
    const cards = await models.card.findAll({ order: Sequelize.literal('random()'), limit: count });
    
    if (cards) {
      return res.status(200).json({ cards });
    }

    return res.status(404).send("Problem fetching random cards");
  } catch (error) {
    return res.status(500).send(error.message);
  }
}

/**
 * Gets the comment(s) for a specific card
 * @param {*} req request from client
 * @param {*} res response from server
 * @returns the comment(s) in JSON format (if found, otherwise 400/500 response code)
 */
const getCommentsOnCard = async (req, res) => {
  try {
    const cardid = req.params.cardid;
    if (cardid) {
      const comments = await models.comment.findAll({
        where: {
          cardid: cardid
        }, include: [models.user]
      });
      // returns array of comments if multiple on card
      return res.status(200).json(comments);
    }
    else {
      // There is no content to send for this request
      return res.status(204).send("No comments found for this card");
    }
  } catch (error) {
    return res.status(500).send(error.message);
  }
};

const addCardToCollection = async (req, res) => {
  //collections/cards/base1-1?userAuth=true&collectionid=3
  if (req.query.userAuth === 'true') { // is user logged in
    try {
      const collectionbridge = await models.collectionbridge.create({
        cardid: req.params.id,
        collectionid: parseInt(req.query.collectionid),
      });
      return res.status(201).send(`Successfully added "${req.params.id}" collection`);
    } catch (error) {
      return res.status(500).send(error.message);
    }
 }
};

const getSomeCards = async (req, res) => {
  try {
    const cards = await models.card.findAll({ limit: 40 })
    return res.status(200).json({ cards });
  } catch (error) {
    return res.status(500).send(error.message);
  }
};

/**
 * Returns 60 cards with a desire offset of 10 times the param given
 * @param {*} req request from client
 * @param {*} res response from server
 * @returns {JSON} contains "metadata" field with the total of cards in 
 * database and returns the offset cards contained in the "paginate" field. 
 */
const paginateBrowseCards = async (req, res) => {
    try{
      //Normalize given parameter and verifies if it is a number
      const normalParam = req.params.num.normalize("NFKC")
      const globalRegex = new RegExp(/[0-9]+/ , 'g')
      if(globalRegex.test(normalParam)){
        const total = await models.card.count()
        const paginate = await models.card.findAll({limit: 30, offset: 30*normalParam})
        result = {
          metadata: {
            total: total 
          },
          paginate
        }
        return res.status(200).json(result)
      }
    }catch(error){
      return res.status(500).send(error)
    }
}

//Might need to change the functions to allow pagination.
/**
 * Searches for all the cards that matches with the given type in the route
 * and gives the total.
 * @param {*} req 
 * @param {*} res 
 * @returns result of all the matching cards
 */
const searchCardByType = async (req, res) => {
  try{
    const normalParam = req.params.type.normalize("NFKC")
    const globalRegex = new RegExp(/[A-Za-z]+/ , 'g')
    if(globalRegex.test(normalParam)){
      const total = await models.card.count({where:{ typing : { [Op.iLike] : {[Op.any] : '%' + normalParam + '%'}}}})
      const paginate = await models.card.findAll({where:{ typing : { [Op.iLike] : {[Op.any] : '%' + normalParam + '%'}}}})
      const result = {
        metadata: {
          total: total 
        },
        paginate
      }
      return res.status(200).json(result)
    }
  }catch(error){
    return res.status(500).json(error)
  }
}

/**
 * returns all the cards that matches with a given set name
 * and gives the total.
 * @param {*} req 
 * @param {*} res 
 * @returns result of query that matches the set name. 
 */
const searchCardBySet = async (req, res) => {
  try{
    const normalParam = req.params.setname.normalize("NFKC")
    const globalRegex = new RegExp(/[A-Za-z]+/ , 'g')
    if(globalRegex.test(normalParam)){
      const test = await models.cardsets.findAll({ where:{
        setname: normalParam
        }
      })
      const total = await models.card.count({where:{ setid : { [Op.iLike] : {[Op.any] : '%' + normalParam + '%'}}}})
      const paginate = await models.card.findAll({where:{ setid : { [Op.iLike] : {[Op.any] : '%' + normalParam + '%'}}}})
      const result = {
        metadata: {
          total: total 
        },
        paginate
      }
      return res.status(200).json(result)
    }
  }catch(error){
    return res.status(500).json(error)
  }
}

/**
 * returns all the cards that matches with the given tags (up to 3)
 * and gives the total.
 * @param {*} req 
 * @param {*} res 
 * @returns result of query that matches the set of tags.
 */
const searchCardByParams = async(req, res) => {
  try{
    const normalParamTags = []
    const normalParamType = []
    const queryArr = Object.entries(req.query)
    const globalRegex = new RegExp(/[A-Za-z0-9]+/)
    const checkTag = new RegExp(/tag[0-2]$/)
    const checkType = new RegExp(/^t$/)
    const page = req.query.page ? req.query.page.normalize("NFKC") : 0
    queryArr.forEach((entry) => {
      tempTag = entry[0].normalize("NFKC")
      tempValue = ""
      if(checkTag.test(tempTag)){
        tempValue = sanitizeTag(entry)
        if(globalRegex.test(tempValue)){
          normalParamTags.push('%' + tempValue + '%')
        }
      }else{
        tempValue = entry[1]        
        if(globalRegex.test(tempValue) && checkType.test(tempTag)){
          if(typeof tempValue === "string"){
            normalParamType.push('%' +tempValue.normalize("NFKC")+ '%')
          }
          else{
            tempValue.forEach((x) => {normalParamType.push('%' + x.normalize("NFKC")+ '%')})
          }
        }
      }
    })
    const total = await models.card.count({where:{ 
    cardname : 
        { [Op.iLike] : normalParamTags.length > 0 ? {[Op.any]: normalParamTags} : "%" }, 
    typing :
      { [Op.iLike] : normalParamType.length > 0 ? {[Op.any]: normalParamType} : "%" }}})

    const paginate = await models.card.findAll({
      limit: 30,
      offset: 30*page, 
      where:{ 
        cardname : 
        { [Op.iLike] : normalParamTags.length > 0 ? {[Op.any]: normalParamTags} : "%" },
        typing :
        { [Op.iLike] : normalParamType.length > 0 ? {[Op.any]: normalParamType} : "%" }
      }
    })

    const result = {
      metadata: {
        total: total,
        tags: normalParamTags.map((x)=>{
          return x.replace(/^%/, '').replace(/%$/, '')
        }),
        types: normalParamType
      },
      paginate
    }

    return res.status(200).json(result)
  }catch(error){
    return res.status(500).json(error)
  }
}

/**
 * Sends 10 possible of what the user might try to find 
 * when inputting in the search bar.
 * @param {*} req 
 * @param {*} res 
 */
const suggestionOfCards = async(req,res) => {
  try{
    const normalInput = req.params.input.normalize("NFKC") + "%"
    const globalRegex = new RegExp(/[A-Za-z0-9]+/)
    const suggestion = await models.card.findAll({attributes:[
      [Sequelize.fn('DISTINCT', Sequelize.col('cardname')) ,'cardname']
    ],
     where:{cardname : {[Op.iLike]: normalInput}}, limit: 10})
    res.status(200).json({cards: suggestion})
  }catch(error){
  }
}

module.exports = {
  getAllCards,
  getCard,
  getRandomCards,
  getCommentsOnCard,
  addCardToCollection,
  getSomeCards,
  paginateBrowseCards,
  searchCardByType,
  searchCardBySet,
  searchCardByParams,
  addCardImage,
  suggestionOfCards,
};