const models = require("../database/models");
const card = require("../database/models/card");

const getAllCollections = async (req, res) => {
  try {
    const collections = await models.collections.findAll();
    if (collections) {
      return res.status(200).json({ collections });
    }
    return res.status(204).send("You currently have no collections");
  } catch (error) {
    return res.status(500).send(error.message);
  }
}

const getFavoriteCollectionCards = async (req, res) => {
  try {
    let favoriteCollection = await models.collections.findOne({
      where: {
        userid: parseInt(req.params.userid),
        ranking: 0
      }
    });

    if(!favoriteCollection) {
      favoriteCollection = await models.collections.findOne({
        where: {
          userid: parseInt(req.params.userid),
        }
      });

      if(!favoriteCollection) return res.status(204).send("No collection found");
    }

    const id = favoriteCollection.collectionid;
    const cardIds = await models.collectionbridge.findAll({
      where: {
        collectionid: id
      }
    });

    const cards = []
    for(element of cardIds) {
      const id = element.dataValues.cardid;
      const card = await models.card.findOne({
        where: {
          cardid: id
        }
      });

      cards.push(card);
    }

    return res.status(200).json({ cards });
  } catch (error) {
    return res.status(500).send(error.message);
  }
};

const getUsersCollections = async (req, res) => {
  try {
    const collections = await models.collections.findAll({
      where: {
        userid: parseInt(req.params.userid)
      },
      order: [
        ['ranking', 'ASC'],
      ]
    });
    if (collections) {
      return res.status(200).json({ collections });
    }
    return res.status(204).send("You currently have no collections");
  } catch (error) {
    return res.status(500).send(error.message);
  }
}

/**
 * Returns the collection matching the collectionid, 
 * while joining the collectionbridge and the card tables
 * 
 * @param {*} req 
 * @param {*} res 
 * @returns result of query that matches the collectionid
 */
const getCollectionById = async (req, res) => {
  try {
    const collections = await models.collections.findAll({
      where: {
        collectionid: parseInt(req.params.collectionid)
      },
      include: {
        model: models.collectionbridge,
        foreignKey: 'collectionid',
        require: true,
        include: {
          model: models.card,
          foreignKey: 'cardid',
          require: true
        }
      },
    });
    if (collections) {
      return res.status(200).json({ collections });
    }
    return res.status(204).send("No collection found with this ID");
  } catch (error) {
    return res.status(500).send(error.message);
  }
}

const addCollection = async (req, res) => {
  // collections?userAuth=keycloak.authenticated&user=x
  if (req.query.userAuth === 'true') { // is user logged in
    try {
      // first get the username from the id
      const  user = await models.user.findOne({ where: { username: req.query.user } });
      if(user.userid){
        // then create a new collection entry
        const collection = await models.collections.create({
          collectionname: req.body.collectionname,
          userid: user.userid,
        });
        if(collection){
          return res.status(201).send(collection);
        }
      }
    } catch (error) {
      return res.status(500).send(`Collection "${req.body.collectionname}" already exists`);
    }
  }
};

/**
 * Deletes the corresponding card from the collectionbridge 
 * table where cardid and collectionid match. Authenticates 
 * the user as well to validate that the proper user has 
 * deletion rights.
 * 
 * @param {*} req 
 * @param {*} res 
 * @returns status of the query as a message
 */
const deleteCardFromCollection = async (req,res) => {
  // collection/:collectionid?userAuth=keycloak.authenticated&delete=true&cardid={id}
  if(req.query.delete && req.query.userAuth === 'true'){
    try{
      const card = await models.collectionbridge.destroy({
        where: {
          collectionid: req.params.collectionid,
          cardid: req.query.cardid
        }
      });
      return res.status(201).send('Succesfully deleted card from collection');
    }catch(error) {
      return res.status(500).send(error.message);
    }
  }
}

const updateCollectionRank = async (req, res) => {
  try {
    const update = models.collections.update(
      { ranking: req.body.rank },
      { where: { collectionid: req.params.id } }
    )
    return res.status(200).send("Collection updated");
  } catch(error) {
    return res.status(500).send(error.message);
  }
}

module.exports = {
  getUsersCollections,
  getFavoriteCollectionCards,
  addCollection,
  getCollectionById,
  getAllCollections,
  deleteCardFromCollection,
  updateCollectionRank
};