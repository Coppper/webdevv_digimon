const models = require("../database/models");

const getUserById = async (req, res) => {
  try {
    const user = await models.user.findOne({ where: { userid: req.params.userid } });
    if (user) {
      return res.status(200).json({ user });
    }
    return res.status(204).send(`User with ${username} not found!`);
  } catch (error) {
    return res.status(500).send(error.message);
  }
};


const getUserByName = async (req, res) => {
  try {
    const user = await models.user.findOne({ where: { username: req.params.username } });
    if (user) {
      return res.status(200).json({ user });
    }
    return res.status(204).send(`User with ${username} not found!`);
  } catch (error) {
    return res.status(500).send(error.message);
  }
};

const editUser = async (req, res) => {
  try {
    models.user.update(
      { location: req.body.location, bio: req.body.bio },
      { where: { userid: req.params.userid } }
    )
    return res.status(200).send("User updated");
  } catch(error) {
    return res.status(500).send(error.message);
  }
}

const addUser = async (req, res) => {
  // add user to db
  try {
    const user = await models.user.findOne({ where: { username: req.body.username }});
    if(user){
      return res.status(200).send(user);
    }
    else{
      const user = await models.user.create({
        username: req.body.username,
      });
      return res.status(201).send(user);
    }
  } catch (error) {
    return res.status(500).send(`Unable to add ${req.body.username}`);
  }
};

module.exports = {
  editUser,
  getUserById,
  getUserByName,
  addUser
}