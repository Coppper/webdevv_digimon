const models = require("../database/models");

const getUserFollows = async (req, res) => {
  try {
    const follow = await models.follow.findAll({ where: { userid: req.params.userid } });
    if (follow) {
      return res.status(200).json({ follow });
    }
    return res.status(204).send("No user matched");
  } catch (error) {
    return res.status(500).send(error.message);
  }
};

const getUserFollowers = async (req, res) => {
  try {
    const follow = await models.follow.findAll({ where: { followid: req.params.followid } });
    if (follow) {
      return res.status(200).json({ follow });
    }
    return res.status(204).send("No user matched");
  } catch (error) {
    return res.status(500).send(error.message);
  }
};

const getUserSpecificFollow = async (req, res) => {
  try {
    const follow = await models.follow.findOne({ where: { userid: req.params.userid, followid: req.params.followid } });
    if (follow) {
      return res.status(200).json({ follow });
    }
    return res.status(204).send("No user matched");
  } catch (error) {
    return res.status(500).send(error.message);
  }
};

const addUserFollow = async (req, res) => {
  try {
    const follow = await models.follow.create({
      userid: req.params.userid,
      followid: req.params.followid,
    });
    
    return res.status(201).send(follow);
  } catch (error) {
    return res.status(500).send(error.message);
  }
};

const deleteUserFollow = async (req, res) => {
  try {
    const follow = await models.follow.destroy({
      where: {
        userid: req.params.userid,
        followid: req.params.followid,
      }
    });

    return res.status(200).send();
  } catch (error) {
    return res.status(500).send(error.message);
  }
};

module.exports = {
  getUserFollows,
  getUserFollowers,
  getUserSpecificFollow,
  addUserFollow,
  deleteUserFollow
};