const models = require("../database/models");

const getAllSets = async (req, res) => {
  try {
    const sets = await models.cardsets.findAll({
    });
    if(sets){
      return res.status(200).json({ sets });
    }
    return res.status(404).json("Unable to retrieve data for all sets");
  } catch (error) {
    return res.status(500).send(error.message);
  }
};

module.exports = {
  getAllSets
};