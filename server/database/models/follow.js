module.exports = (sequelize, DataTypes) => {
    const follow = sequelize.define('follow', {
        userid: {
            type: DataTypes.INTEGER,
            allowNull: false,
            primaryKey: true
        },
        followid: {
            type: DataTypes.INTEGER,
            allowNull: false
        }
    }, 
    { timestamps: false }); // timestamps turned off, otherwise 'createdat + modifiedat' column get created
    
    return follow;
};