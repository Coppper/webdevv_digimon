module.exports = (sequelize, DataTypes) => {
    const ratings = sequelize.define('rating', {
        rating: {
            type: DataTypes.DECIMAL,
        },
        cardid: {
            type: DataTypes.STRING,
            allowNull: false
        },
    }, { timestamps: false });
    ratings.associate = function (models) {
        ratings.hasMany(models.card, { foreignKey: 'cardid', sourceKey: 'cardid' });
    };
    ratings.removeAttribute('id'); // no need for sequelized primarykey since this is a bridging table
    return ratings;
};