module.exports = (sequelize, DataTypes) => {
    const cardsets = sequelize.define('cardsets', {
        setid: {
            type: DataTypes.STRING,
            primaryKey: true,
            allowNull: false
        },
        setname: {
            type: DataTypes.STRING,
            allowNull: false
        },
        series: {
            type: DataTypes.STRING,
            allowNull: false
        },
        total: {
            type: DataTypes.STRING,
            allowNull: false
        },
        releasedate: {
            type: DataTypes.STRING,
            allowNull: false
        },
        setapilink: {
            type: DataTypes.STRING,
            allowNull: true
        },
    }, { timestamps: false }); // timestamps turned off, otherwise 'createdat + modifiedat' column get created
    cardsets.associate = function (models) {
        cardsets.hasMany(models.card, { foreignKey: 'cardid' });
    };
    return cardsets;
};