module.exports = (sequelize, DataTypes) => {
    const collectionbridge = sequelize.define('collectionbridge', {
        cardid: {
            type: DataTypes.STRING,
            allowNull: false
        },
        collectionid: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        // timestamps turned off, otherwise 'createdat + modifiedat' column get created
        // freeze table name otherwise sequelize will pluralize the table name
    }, { timestamps: false, freezeTableName: true }); 
    collectionbridge.associate = function (models) {
        collectionbridge.hasMany(models.card, { foreignKey: 'cardid', sourceKey: 'cardid' });
        collectionbridge.belongsTo(models.collections, { foreignKey: 'collectionid', sourceKey: 'collectionid' });
    };
    collectionbridge.removeAttribute('id'); // no need for sequelized primarykey since this is a bridging table
    return collectionbridge;
};