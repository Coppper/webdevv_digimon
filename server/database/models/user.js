module.exports = (sequelize, DataTypes) => {
    const user = sequelize.define('user', {
        userid: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true // Automatically gets converted to SERIAL for postgres
          },
        bio: {
            type: DataTypes.STRING
        },
        location: {
            type: DataTypes.STRING
        },
        username: {
            type: DataTypes.STRING,
            unique: true
        },
        profilepic: {
            type: DataTypes.STRING
        },
        bannerpic: {
            type: DataTypes.STRING
        },
        profilepic: {
            type: DataTypes.STRING
        },
        bannerpic: {
            type: DataTypes.STRING
        },
    }, { timestamps: false }); // timestamps turned off, otherwise 'createdat + modifiedat' column get created
    return user;
};