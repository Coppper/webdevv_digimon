module.exports = (sequelize, DataTypes) => {
    const card = sequelize.define('card', {
        cardid: {
            type: DataTypes.STRING,
            primaryKey: true,
        },
        setid: {
            type: DataTypes.STRING,
            allowNull: false
        },
        cardname: {
            type: DataTypes.STRING,
            allowNull: false
        },
        cardtype: {
            type: DataTypes.STRING,
            allowNull: false
        },
        subtype: {
            type: DataTypes.STRING,
            allowNull: false
        },
        typing: {
            type: DataTypes.STRING,
            allowNull: false
        },
        evolvesto: {
            type: DataTypes.STRING,
            allowNull: false
        },
        evolvesfrom: {
            type: DataTypes.STRING,
            allowNull: false
        },
        rarity: {
            type: DataTypes.STRING,
            allowNull: false
        },
        flavortext: {
            type: DataTypes.STRING,
            allowNull: false
        },
        nationaldex: {
            type: DataTypes.STRING,
            allowNull: false
        },
        artist: {
            type: DataTypes.STRING,
            allowNull: false
        },
        cardapilink: {
            type: DataTypes.STRING,
            allowNull: true
        },
    }, { timestamps: false }); // timestamps turned off, otherwise 'createdat + modifiedat' column get created
    // association aka foreign key
    card.associate = function (models) {
        card.belongsTo(models.cardsets, { foreignKey: 'setid' })
    };
    return card;
};