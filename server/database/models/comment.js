module.exports = (sequelize, DataTypes) => {
    const comment = sequelize.define('comment', {
        commentid: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        cardid: {
            type: DataTypes.STRING,
            allowNull: false
        },
        userid: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        content: {
            type: DataTypes.STRING
        }
    }, { timestamps: false }); // timestamps turned off, otherwise 'createdat + modifiedat' column get created
    // association aka foreign key
    comment.associate = function (models) {
        comment.belongsTo(models.card, { foreignKey: 'cardid' })
    };
    comment.associate = function (models) {
        comment.belongsTo(models.user, { foreignKey: 'userid' })
    };
    return comment;
};