module.exports = (sequelize, DataTypes) => {
    const collections = sequelize.define('collections', {
        collectionid: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        collectionname: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        userid: {
            type: DataTypes.INTEGER,
            allowNull: false,
            unique: true
        },
        ranking: {
            type: DataTypes.INTEGER,
            allowNull: true
        }
    }, { timestamps: false }); // timestamps turned off, otherwise 'createdat + modifiedat' column get created
    collections.associate = function (models) {
        collections.belongsTo(models.user, { foreignKey: 'userid' });
        collections.hasMany( models.collectionbridge, {foreignKey: 'collectionid'});
    };
    return collections;
};