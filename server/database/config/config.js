module.exports = {
  development: {
    url: process.env.DATABASE_URL,
    dialect: 'postgres',
    "dialectOptions": {
      "ssl": {
         "require": true,
      }
    }
  },
  test: {
    url: process.env.TEST_DATABASE_URL,
    dialect: 'postgres',
    "ssl": true,
    "dialectOptions": {
       "ssl": {
          "require": true,
       }
     }
  },
  production: {
    use_env_variable: "DATABASE_URL",
    dialect: 'postgres',
    "ssl": true,
    "dialectOptions": {
       "ssl": {
          "require": true,
          "rejectUnauthorized": false,
       }
     }
  },
};
