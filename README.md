---
title:  'Pokemon Collection Site - PERN Application deployed on Heroku'
authors: (Merck Inc.)
- Rony-Estefan Maheux-Saban
- Kristina Amend
- Cooper Slutsken
- Michael Faria
- Étienne Plante
---

# Overview

https://pokellection.herokuapp.com/

Pokemon Cardlogue - Fullstack PERN application<br>

# Features

## Browse
- Guests and authenticated users can browse all 14410 on one convenient page
- Users can search cards by keywords or filter by type
<img src="https://poke-collection-staging.herokuapp.com/static/media/Tile1_HomePage.0f7ff256816c5b91d358.png" alt="browsefeature" width="400"/>

## Details
- Guests and authenticated can view details for a specific card
<img src="https://poke-collection-staging.herokuapp.com/static/media/Tile2_HomePage.0e4613d802ea75b6b989.png" alt="browsefeature" width="400"/>

## Details (Logged in users)
- Authenticated users can rate, comment, and add a card to a specific collection
<img src="https://poke-collection-staging.herokuapp.com/static/media/Tile3_HomePage.1998aa7fc17902b807d4.png" alt="browsefeature" width="400"/>

## Collections
- Authenticated users can keep track of all their collected cards by adding them to their own created collections
<img src="https://poke-collection-staging.herokuapp.com/static/media/Tile4_HomePage.a6993659418012064c3e.png" alt="browsefeature" width="400"/>

## Elements of the PERN application

### ![](https://img.icons8.com/color/48/000000/postgreesql.png) PostgreSQL 
- Hosted on Azure Database for PostgreSQL flexible server
- Used to store all information for to cards, card sets, users, user collections, card comments, card ratings
### ![](https://img.icons8.com/color/48/000000/javascript--v1.png) Express 
- Back end web application framework for API
### ![](https://img.icons8.com/color/48/000000/react-native.png) React  &  ![](https://img.icons8.com/color/48/000000/material-ui.png) MUI
- React used in conjuction with MUI for displaying the front end
### ![](https://img.icons8.com/fluency/48/000000/node-js.png) Node.js 
- Back end js runtime environment for database operations and tests

## Backend

### ![](https://img.icons8.com/color/48/000000/java-coffee-cup-logo--v1.png) Java
- Used for to download github API json data and populate the PostgreSQL database
### ![](https://design.jboss.org/keycloak/logo/images/keycloak_icon_32px.png) Keycloak
- Authentication management for login, registration

# Steps to run

## Database setup (Create SQL tables, parse JSON, populate database)

1. Open project in preferred IDE
2. Add JDBC connection in IDE with provided credentials
3. Navigate to WebDebDbSetup -> test
    -  Enter database credentials in DBTests.s
4. Build maven project in preferred IDE
5. Navigate to WebDebDbSetup -> src
6. Run main (JSONFetch)
7. Enter credentials in:
    - JSONFetch - username, password (via the console)
8. Practice patience as this could take hours depending on your system and network specfications

## Application setup

#### Step 0 (Setup)

1. Navigate to the root of the project and add .env file
2. Navigate to /server and add .env file
3. Navigate to /root and **npm i**, navigate to /server and **npm i**, navigate to /client and **npm i**

### Step 1 (Tests)

1. Navigate to /server/_ _tests_ _ and run the following commands
    - **npm test** browse
    - **npm test** carview
    - **npm test** collections
    - **npm test** lists
    - **npm test** profilepage

### Step 2 Run on localhost (Development)

0. Open two terminals
1. terminal 1: cd to /server & **node server**
2. terminal 2: cd to /client & **npm start**
3. Open your preferred browser and navigate to [http://localhost:3000/]
4. Register to create an account to benefit from the applications features

### Step 3 View final application in production

https://pokellection.herokuapp.com/


### Useful links

[API Docs](http://localhost:3001/docs/) (Localhost only) <br>
[Dataset (API) source](https://github.com/PokemonTCG/pokemon-tcg-data)<br>
[App icons source](https://icons8.com/) - [Keycloak icon](https://design.jboss.org)
