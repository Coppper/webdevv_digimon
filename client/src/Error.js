import React from 'react';
import { Link } from 'react-router-dom';
import { Button } from '@mui/material';
import error404 from "./img/error404.png"

// image sources:
// https://www.flaticon.com/free-icon/open-pokeball_188917
// https://www.flaticon.com/free-icon/pikachu_188939

const NotFound = () => (
  <div style={{marginTop: "60px"}}>
    <img src={error404} alt="error 404" />
    <Link to="/" style={{ textDecoration: "none" }}><br></br><br></br>
    <Button variant="contained" color="secondary" disableElevation>Go Home</Button>
    </Link>
  </div>
);

export default NotFound;