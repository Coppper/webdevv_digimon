import React from 'react';
import Modal from '@mui/material/Modal';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import { Autocomplete, Box, Stack , Select, MenuItem} from '@mui/material';
import CircularProgress from '@mui/material/CircularProgress';
import SvgIcon from '@mui/material/SvgIcon';
import { width } from '@mui/system';
import {useNavigate} from 'react-router-dom';


//https://icomoon.io/

export default function SearchModal(props) {
  const navigate = useNavigate();
  const limitTagVal = 3;
  const labelTxtField = 'Add Tag';
  const [inputError, setInputError] = React.useState(false)
  const [open, setOpen] = React.useState(false);
  const [options, setOptions] = React.useState([]);
  const [query, setQuery] = React.useState([]);
  const [currentDelay, setCurrentDelay] = React.useState();
  const [currentInput, setCurrentInput] = React.useState([]);
  const [searching, setSearching] = React.useState(false)
  const loading = open && searching;

  const modalStyle = {
    position: 'absolute',
    top: '15%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    background: '#18272c', // slate grey
    borderRadius: '5px',
    borderSpacing: 0,
    borderCollapse: 'separate'
  };

  /**
   * reset states when query succesfully process
   */
  const resetStates = () =>
  {
    setInputError(false)
    setQuery([])
    props.closeEvent()
  } 

  /**
   * Search function to navigate at the browse with
   * the selected tags.
   */
  const searchFunc = () => {
    if(query.length > 0){
    let urlquery = "?"
    let tempquery = "tag"+0+"="+(query.at(0).cardname ? encodeURIComponent(query.at(0).cardname) : encodeURIComponent(query.at(0)))
    urlquery = urlquery.concat(tempquery)

    for(let i = 1; i < query.length;i++){
      let tempquery = "tag"+i+"="+(query.at(i).cardname ? encodeURIComponent(query.at(i).cardname) : encodeURIComponent(query.at(i)))
      urlquery = urlquery.concat("&",tempquery) 
    }
    navigate("/browse/1" + urlquery)
    resetStates()
    
    }
    else
    {
      setInputError(true)
    }
  }
  
  /**
   * Checks if the number of tags reach the limits.
   * Shifts the list by removing the first element to place the new last element.
   * @param {Event} e 
   * @param {list of tags contained in the autocomplete} tags 
   */
  const checkLimit = (e,tags) => {
    if(tags.length > limitTagVal)
      tags.shift()
    setQuery(tags)
  }

  /**
   * https://stackoverflow.com/questions/61946587/how-to-cancel-javascript-sleep
   * function to delay the fetch when user is typing.
   * @param {millisecond to delay} ms 
   */
  function delayInput(ms){
    let timeout_id;
    let rejector;
    const prom = new Promise((resolve, reject) => {
      rejector = reject;
      timeout_id = setTimeout(() => {
        resolve();
        setSearching(true)
        setOptions([])
      }, ms);
    });
    prom.abort = () => {
    clearTimeout( timeout_id );
    };
    return prom;
}

/**
 * Creates timer when user input in the search bar
 * if the user inputs again the timer resets.
 * if user stop inputs, it fetches suggestion for autocomplete.
 * @param {} value 
 * @param {current input of user} reason 
 */
const handleCurrentInput = async (value, reason) => {
  if(currentDelay instanceof Promise){
    currentDelay.abort()
  }
  setCurrentInput(reason)
  setCurrentDelay(delayInput(250))

}

/**
 * Handles the "Enter" key to trigger a search
 * when the user is done adding the desire tags.
 * @param {event} e 
 */
const handleEnterKey = (e) => {
  // 13 = enter key
  if (e.keyCode === 13 && query.length > 0 && currentInput.length === 0) {
    e.stopPropagation()
    searchFunc()    
    e.stopPropagation()
  }
};

/**
 * fetches suggestion to user when searching is set to true
 */
React.useEffect(async () => {
  let listOfOptions
  listOfOptions = await (await fetch("/api/cards/suggestion/" + currentInput)).json()
  .then(setSearching(false))
  setOptions(listOfOptions.cards)
}, [searching])

/**
 * resets options when user close the modal.
 */
React.useEffect(() => {
  if (!open) {
    setOptions([]);
  }
}, [open]);

  return (<React.Fragment>
          <Modal
            disableEnforceFocus
            onClick={event => event.stopPropagation()}
            onKeyDown={event => event.stopPropagation()}
            onMouseDown={event => event.stopPropagation()}
            open={props.openEvent}
            onClose={props.closeEvent}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
          >
            <Stack  direction="row" sx={modalStyle}>
              <Autocomplete
              multiple
              freeSolo
              onChange={checkLimit}
              open={open}
              onOpen={() => {
                setOpen(true);
              }}
              onClose={() => {
                setOpen(false);
              }}
              getOptionLabel={(option) => option.cardname || option}
              options={options}
              loading={loading}
              onInputChange={handleCurrentInput}
              renderInput={(params) => (
              <TextField {...params} sx={{width: 500}} error={inputError}
              onKeyDown={handleEnterKey}
              color="secondary" id="filled-basic" label={labelTxtField} variant="filled"
              helperText="Add name tag of a pokemon. I.e: Charizard"
              InputProps={{
                ...params.InputProps,
                endAdornment: (
                  <React.Fragment>
                    {loading ? <CircularProgress color="inherit" size={20} /> : null}
                    {params.InputProps.endAdornment}
                  </React.Fragment>
                ),
              }}
              />
              )}
              />
            <Button variant="text disableElevation" onClick={searchFunc}>Search</Button>
            </Stack>
          </Modal>
          </React.Fragment>);
}

const test = [
  {Tagname: "Gardevoir"},
  {Tagname: "Charizard"},
  {Tagname: "Mimikyu"}
]