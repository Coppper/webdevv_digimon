import * as React from "react";
import { Link } from "react-router-dom";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Menu from "@mui/material/Menu";
import MenuIcon from "@mui/icons-material/Menu";
import Container from "@mui/material/Container";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import Tooltip from "@mui/material/Tooltip";
import MenuItem from "@mui/material/MenuItem";
import SearchIcon from "@mui/icons-material/Search";
import logo from "../../img/logo.png";
import { useKeycloak } from "@react-keycloak/web";
import checkUser from "../../Helpers/checkUser";
import SearchModal from "./SearchModal.jsx";
import AccountCircleIcon from '@mui/icons-material/AccountCircle';

const settings = ["Lists"];

const Navbar = () => {
  const [anchorElNav, setAnchorElNav] = React.useState(null);
  const [anchorElUser, setAnchorElUser] = React.useState(null);

  const { keycloak, initialized } = useKeycloak();
  let keycloakUsername = keycloak.idTokenParsed?.preferred_username;

  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };
  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  const [open, setOpen] = React.useState(false);
  const handleOpenSearchModal = () => {setOpen(true);};
  const handleCloseSearchModal = () => {setOpen(false);};


  return (
    <AppBar position="static" sx={{ px: "5px", marginBottom: "30px", background: "#8b3c3d ! important"}}>
      {/*Main navbar*/}
      <Container maxWidth="xl">
        <Toolbar disableGutters>
          <Typography
            variant="h6"
            noWrap
            component="div"
            sx={{ mr: 2, display: { xs: "none", md: "flex" } }}
          >
            <Link to="/" style={{ textDecoration: "none"}}>
              <img src={logo} width="38px" alt="logo" />
            </Link>
          </Typography>
          <Box sx={{ flexGrow: 1, display: { xs: "flex", md: "none" } }}>
            <IconButton
              size="large"
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={handleOpenNavMenu}
              color="inherit"
            >
              <MenuIcon />
            </IconButton>
            <Menu
              id="menu-appbar"
              anchorEl={anchorElNav}
              anchorOrigin={{
                vertical: "bottom",
                horizontal: "left",
              }}
              keepMounted
              transformOrigin={{
                vertical: "top",
                horizontal: "left",
              }}
              open={Boolean(anchorElNav)}
              onClose={handleCloseNavMenu}
              sx={{
                display: { xs: "block", md: "none" },
              }}
            >
              <MenuItem onClick={handleCloseNavMenu}>
                <Link to="/" style={{ textDecoration: "none" }}>
                  <Typography textAlign="center">HOME</Typography>
                </Link>
              </MenuItem>
              <MenuItem onClick={handleCloseNavMenu}>
                <Link to={`browse/1`} style={{ textDecoration: "none" }}>
                  <Typography textAlign="center">BROWSE</Typography>
                </Link>
              </MenuItem>
            </Menu>
          </Box>
          <Box sx={{ flexGrow: 1, display: { xs: "none", md: "flex" } }}>
            {keycloak.authenticated && (
            <Link to={`lists`} style={{ textDecoration: "none"}}>
              <Button
                onClick={handleCloseNavMenu}
                sx={{ my: 2, color: "white", display: "block", fontSize: "1.05em", px: "10px" }}
              >
                LISTS
              </Button>
            </Link>)
            }
            
            <Link to={`browse/1`} style={{ textDecoration: "none"}}>
              <Button
                onClick={handleCloseNavMenu}
                sx={{ my: 2, color: "white", display: "block", fontSize: "1.05em", px: "10px" }}
              >
                BROWSE
              </Button>
            </Link>
          </Box>

          <Box sx={{ flexGrow: 0 }}>
            <Box sx={{ display: "flex", flexDirection: "row" }}>
              {/* SEARCH BUTTON */}
              <IconButton
                onClick={handleOpenSearchModal}
                size="large"
                aria-label="search"
                color="inherit"
              >
                <SearchIcon />
              </IconButton>

              {/* User icon when authenticated */}
              {keycloak.authenticated && (
                <div>
                  <Tooltip title="Open settings">
                    <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                      <AccountCircleIcon sx={{color: "#232323", fontSize: "xxx-large"}} />
                    </IconButton>
                  </Tooltip>
                  <Menu
                    sx={{ mt: "45px" }}
                    id="menu-appbar"
                    anchorEl={anchorElUser}
                    anchorOrigin={{
                      vertical: "top",
                      horizontal: "right",
                    }}
                    keepMounted
                    transformOrigin={{
                      vertical: "top",
                      horizontal: "right",
                    }}
                    open={Boolean(anchorElUser)}
                    onClose={handleCloseUserMenu}
                  >
                    <MenuItem
                      onClick={() => {
                        handleCloseUserMenu();
                        keycloak.logout();
                      }}
                    >
                      <Typography textAlign="center">Logout</Typography>
                    </MenuItem>
                  </Menu>
                </div>
              )}
              {/* LOGIN/REGISTER BUTTONS WHEN NOT LOGGED IN */}
              {!keycloak.authenticated && (
                <MenuItem onClick={() => keycloak.login()}>
                  <Typography sx={{ fontSize: "1.05em" }} textAlign="center">LOGIN</Typography>
                </MenuItem>
              )}
              {!keycloak.authenticated && (
                <MenuItem onClick={() => keycloak.register()}>
                  <Typography sx={{ fontSize: "1.05em" }} textAlign="center">REGISTER</Typography>
                </MenuItem>
              )}
            </Box>

            {keycloak.authenticated && checkUser(keycloak) && (
              <Menu
                sx={{ mt: "45px" }}
                id="menu-appbar"
                anchorEl={anchorElUser}
                anchorOrigin={{
                  vertical: "top",
                  horizontal: "right",
                }}
                keepMounted
                transformOrigin={{
                  vertical: "top",
                  horizontal: "right",
                }}
                open={Boolean(anchorElUser)}
                onClose={handleCloseUserMenu}
              >
                <Link
                  to={`profile/${keycloakUsername}`}
                  style={{ textDecoration: "none", color: "inherit" }}
                >
                  <MenuItem onClick={handleCloseUserMenu}>
                    <Typography px="12px" textAlign="center">Profile</Typography>
                  </MenuItem>
                </Link>

                <MenuItem onClick={() => keycloak.logout()}>
                  <Typography px="12px" textAlign="center">Logout</Typography>
                </MenuItem>
              </Menu>
            )}
          </Box>
        </Toolbar>
      </Container>

      <SearchModal openEvent={open} closeEvent={handleCloseSearchModal} />
    </AppBar>
  );
};
export default Navbar;
