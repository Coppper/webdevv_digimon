/**
 * Async function that retrieves all collections from a user
 * @param {*} username string representing a user
 * @returns an object containing all of the users collections
 */

export const getUserIdFromUsername = async(username) => {
  let userUrl = `/api/users/username/${username}`;
  const response = await fetch(userUrl);
  if(!response.ok){
    // change to more graceful error handling
    const message = `An error has occured retrieving user info from ${username} : ${response.status}`;
    throw new Error(message);
  }
  const userData = await response.json();
  // return the userid
  return userData.user.userid;
}
export const fetchCollectionsFromUser = async(username) =>{
  if(!username) return;
  
  const userid = await getUserIdFromUsername(username);
  if(userid){
    let collectionUrl = `/api/collections/${userid}`;
    const response = await fetch(collectionUrl);
    if(!response.ok){
      // change to more graceful error handling
      const message = `An error has occured retrieving collections from ${username} : ${response.status}`;
      throw new Error(message);
    }
    const collections = await response.json();
    return await collections.collections;
  }
};


