import * as React from 'react';
import { Grid, Button, Paper, Box, Typography, Tooltip } from '@mui/material';
import styles from "../DetailsPage/card.styles"
import { useKeycloak } from '@react-keycloak/web';
import DeleteCardDialog from './DeleteCardDialog';
import { useContext } from "react";
import { SnackbarContext } from "../../../context/SnackbarContext";

export default function CollectionDetailView(props) {
    // set snackbar for alerts
    const { setSnackbar } = useContext(SnackbarContext);

    //card that is to be displayed
    const [card, setCard] = React.useState([]);

    //image link of the current card
    const [img, setImg] = React.useState('');

    //username of the owner of the collection
    const [username, setUsername] = React.useState([]);

    //used to authenticated and check the username of current user
    const { keycloak, initialized } = useKeycloak();
    let keycloakUsername = keycloak.idTokenParsed?.preferred_username;

    //image url for empty card 
    const emptyImg = "https://pokemonblobs.blob.core.windows.net/images/undefined/undefined";

    //set the username of the owner and the card to be displayed when the props change
    React.useEffect(() => {
        setCard(props.props);
        setUsername(props.username);
        setImg(props.deleteImage);
    }, [props]);

    // state for opening/closing delete card dialog
    const [open, setOpen] = React.useState(false);
    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    /**
 * Deletes the current card that is selected and displays a small message 
 * confirming the deletion while also updating the page
 */
    async function handleDelete() {
        // close the confirmation dialog
        setOpen(false);
        //sends a DELETE request to the server
        let deleteCardUrl = `/api/collection/${props.collectionid}?userAuth=${keycloak.authenticated}&delete=true&cardid=${card.cardid}`;
        const request = await fetch(deleteCardUrl, {
            method: "DELETE",
            headers: { "Content-Type": "application/json" },
        });
        if (!request.ok) {
            const errMessage = `An unexpected error occured, your card was not deleted`;
            // snackbar error alert
            setSnackbar(errMessage, "error");
        }
        const successMessage = `Successfully deleted card`;
        // snackbar success alert
        setSnackbar(successMessage, "success");
        // update the cards
        props.update();
    }

    return (
        <Box maxWidth="350px">

            <Grid color={"white"}
                margin={0}
                elevation={2}
                columns={6}
                container
                direction="row"
                alignItems="center"
                justifyContent="center"
                py={"20px"}
                paddingTop={0}
                paddingBottom={0}
                height={"auto"}
                style={{ backgroundColor: "#2b2b2b" }}
            >

                {/*Display the name of the card selected */}
                <Grid item xs={6} paddingTop={1} paddingBottom={1}>
                    <Paper elevation={0} sx={{ backgroundColor: "#3f3f3f", color: "#FFFFFF" }}>
                        {card.length === 0 && (
                            <Tooltip arrow title={<h2>No Card Selected</h2>}>
                                <Typography padding={"6px"} variant="h4" sx={styles.cardname}>No Card Selected</Typography>
                            </Tooltip>
                        )}
                        {card.length !== 0 && (
                            <Tooltip arrow title={<h2>{card.cardname}</h2>}>
                                <Typography padding={"6px"} variant="h4" sx={styles.cardname}>{card.cardname}</Typography>
                            </Tooltip>
                        )}
                    </Paper>
                </Grid>

                {/*Display the type and subtype of the card */}
                <Grid item xs={5} paddingBottom={1}>
                    <Paper sx={{ backgroundColor: "#12658e", color: "#FFFFFF" }}>
                        <Typography variant="h6">
                            {card.cardtype} {card.subtype}
                        </Typography>
                    </Paper>
                </Grid>

                {/*Display the image of the card thats selected */}
                <Grid item xs={6} paddingBottom={1}>
                    <Paper style={{ height: "auto", width: "100%" }}>
                        {/* if card is empty render undefinied card image */}
                        {card.length === 0 && (
                            <img alt={"No card selected"} src={emptyImg} style={{ display: 'block', margin: 'auto', width: "100%" }} />
                        )}
                        {card.length !== 0 && (
                            <img alt={card.cardname} src={card.cardapilink} style={{ display: 'block', margin: 'auto' }} />
                        )}
                    </Paper>
                </Grid>

                {/*Display flavor text if there is any */}
                {card.flavortext !== "No Card Text" &&
                    <Grid item xs={6} sx={{ maxHeight: "160px", overflow: 'auto' }} paddingBottom={1}>
                        <Typography
                            style={styles.description}
                            paragraph={true}
                        >
                            {card.flavortext}
                        </Typography>
                    </Grid>
                }
                {/*Display the delete button if user is owner of list and if a card is selected */}
                {keycloak.authenticated && keycloakUsername === username && card.length !== 0 && (
                    <Grid item xs={1}>
                        <Button onClick={handleClickOpen}>
                            <img src={img} alt={'Delete from List'} style={{ height: "auto", width: '100%' }} />
                        </Button>
                        {/* Pops up a dialog to confirm card deletion */}
                        <DeleteCardDialog
                            handleClose={handleClose}
                            handleDelete={handleDelete}
                            collection={props.collection}
                            open={open}
                            card={card}>
                        </DeleteCardDialog>
                    </Grid>
                )}
            </Grid>
        </Box>
    );
}