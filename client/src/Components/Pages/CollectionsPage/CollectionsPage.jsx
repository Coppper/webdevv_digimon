import * as React from 'react';
import { useParams } from "react-router-dom";
import GridList from "../../GridComponents/GridList"
import CollectionDetailView from './CollectionDetailCard';
import { Grid, Box, Typography } from '@mui/material';
import deleteImg from "../../../img/delete.png";
import { default as ImageFetch } from "../../../Helpers/imageFetch";
import { useKeycloak } from "@react-keycloak/web";
import { Button } from '@mui/material';
import { Link } from '@mui/material';

/**
 * This method sends an update request to our api to
 * update our database with the api link to limit
 * the amount of http requests we make
 * @param {*} set 
 * @param {*} id 
 */
function updateImage(set, id) {
    const imgId = set + "-" + id;
    const imgLink = { cardapilink: `https://pokemonblobs.blob.core.windows.net/images/${set}/${id}` };
    fetch(`/api/cards/upload/${imgId}`, {
        method: 'POST',
        mode: 'same-origin',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(imgLink),
    })
        .then(response => response.json())
        .then(data => {
        })
        .catch((error) => {
        });
}

export default function CollectionsPage() {
    // initialize the keycloak object
    const { keycloak } = useKeycloak();
    let keycloakUsername = keycloak.idTokenParsed?.preferred_username;

    let params = useParams();
    //stores all the cards
    const [data, setData] = React.useState([]);
    //stores the selected card
    const [card, setCard] = React.useState([]);
    //stores the username of the owner of the collection
    const [username, setUsername] = React.useState("");
    // set collection name
    const [collection, setCollection] = React.useState("");

    /**
     * Check the URL exists in the blob storage. If it does 
     * update the image url if not fetch the image using ImageFetch
     * 
     * @param {*} setID 
     * @param {*} cardID 
     * @returns imageurl
     */
    function checkURL(setID, cardID) {
        var url = `https://pokemonblobs.blob.core.windows.net/images/${setID}/${cardID}`;
        var http = new XMLHttpRequest();
        http.open("HEAD", url, false);
        http.setRequestHeader("Access-Control-Allow-Origin", "*");
        http.send();
        if (http.status != 200) {
            // page did not load
            ImageFetch(setID, cardID).then((apiURL) => { return apiURL });
        } else {
            updateImage(setID, cardID);
            return url;
        }
    }

    /**
     * Loop through the array of carddata to check if it 
     * contains the apilink for the card images
     * 
     * @param {*} carddata 
     */
    function modifyData(carddata) {
        carddata.forEach(element => {
            if (element.cardapilink === null) {
                element.cardapilink = checkURL(element.setid, element.cardid.split("-")[1]);;
            }
        });
        setData(carddata);
    }

    /**
     * Retrieve the owner of the collection using the userid
     * and setting the username from the fetched data
     * 
     * @param {*} userid 
     */
    function getOwnerOfCollection(userid) {
        fetch('/api/users/' + userid)
            .then(response => response.json())
            .then((data) => {
                setUsername(data.user.username.toString());
            })
    }

    //fetch the data from the server
    React.useEffect(() => {
        fetchCardsInCollection();
    }, []);

    /**
     * Fetch the cards from the params.id representing the collection,
     * retrieve the owner of the collection as well with getOwnerOfCollection()
     */
    const fetchCardsInCollection = async () => {
        fetch('/api/collection/' + params.id)
            .then(response => response.json())
            .then((data) => {
                //calls modifyData
                modifyData(data.collections[0].collectionbridges[0].cards)
                // sets the collection name
                setCollection(data.collections[0].collectionname)
                //sets the selected card
                setCard([]);
                //calls getOwnerOfCollection
                getOwnerOfCollection(data.collections[0].userid)
            })
    }

    /**
     * Loops through the array of card objects to find 
     * the one matching the selected id, setting that card
     * to a variable
     * 
     * @param {*} id 
     */
    function handleItemClick(id) {
        //display card in grid
        for (let i = 0; i < data.length; i++) {
            if (data[i].cardid === id) {
                setCard(data[i]);
                break;
            }
        }
    }

    console.log(data)

    return (
        <div style={{ height: 650, width: '95%', margin: "auto" }}>
            <Grid direction="row" flexWrap="nowrap" container spacing={3} columns={12}>
                <Grid item xs={6} md={8} lg={9} xl={9}>
                    {/*Displayed if the collection is empty and the user is the owner*/}
                    {data.length === 0 && keycloakUsername === username && (
                        <Box width={"70%"} margin="auto" marginTop="4vh">
                            <Typography variant="h3"
                                sx={{ textAlign: "center" }}>
                                No cards in your Collection!
                            </Typography>
                            <Typography variant="h5"
                                sx={{ textAlign: "center" }}>
                                Head to Browse to start adding cards to this Collection.
                            </Typography>
                            {/* Add more cards button */}
                            <Link href="/browse/1" style={{ textDecoration: "none" }}>
                                <Box mt="5vh" >
                                    <Button variant="contained" color="secondary">
                                        Add more cards
                                    </Button>
                                </Box>
                            </Link>
                        </Box>
                    )}
                    { }
                    {data.length === 0 && keycloakUsername !== username && (
                        <Box width={"100%"}>
                            <Typography variant="h3"
                                sx={{ textAlign: "left" }}>
                                No cards in this Collection!
                            </Typography>
                        </Box>
                    )}
                    {/*Displayed if the collection isn't empty */}
                    {data.length > 0 && (
                        <><Box mb="3vh" >
                            {/* Collection name */}
                            <Typography variant="h4"
                                sx={{
                                    textAlign: "center",
                                    textShadow: "-1px -1px 1px rgba(255,255,255,.1), 1px 1px 1px rgba(82,110,133)"
                                }}>
                                {collection}
                            </Typography>
                        </Box>
                            <GridList cards={data} onClick={(id) => handleItemClick(id)} />
                            {/* Add more cards button */}
                            <Link href="/browse/1" style={{ textDecoration: "none" }}>
                                <Box mt="5vh" >
                                    <Button variant="contained" color="secondary">
                                        Add more cards
                                    </Button>
                                </Box>
                            </Link></>
                    )}
                </Grid>
                <Grid sx={{ paddingTop: "20px !important " }} item xs={6} md={4} lg={3} xl={3}>
                    <CollectionDetailView
                        props={card}
                        deleteImage={deleteImg}
                        username={username}
                        collectionid={params.id}
                        collection={collection}
                        update={fetchCardsInCollection} />
                </Grid>
            </Grid>
        </div>
    );

}