import { Dialog, DialogTitle, DialogActions, DialogContent, DialogContentText, Button } from "@mui/material";

/**
 * React component that displays a dialog to confirm is a user wants to delete a card
 * @param {*} props 
 * @returns 
 */
export default function DeleteCardDialog(props) {
    const { handleClose, handleDelete, open, card, collection } = props;

    return (
        <div>
            <Dialog
                maxWidth={"xs"}
                fullWidth={true}
                onClose={handleClose}
                open={open}
            >
                <DialogTitle id="responsive-dialog-title">
                    {`Delete Card`}
                </DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        {`Are you sure you want to delete '${card.cardname}' from '${collection}'?`}
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button autoFocus onClick={handleClose}>
                        Go back
                    </Button>
                    <Button onClick={handleDelete} autoFocus>
                        Confirm
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}