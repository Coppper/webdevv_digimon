import React, { } from "react";
import { Grid, Box, Paper, Typography, Tooltip } from "@mui/material";
import colorless from "../../../img/energy/colorless.png"
import darkness from "../../../img/energy/darkness.png"
import dragon from "../../../img/energy/dragon.png"
import fairy from "../../../img/energy/fairy.png"
import fighting from "../../../img/energy/fighting.png"
import fire from "../../../img/energy/fire.png"
import grass from "../../../img/energy/grass.png"
import lightning from "../../../img/energy/lightning.png"
import metal from "../../../img/energy/metal.png"
import other from "../../../img/energy/other.png"
import psychic from "../../../img/energy/psychic.png"
import water from "../../../img/energy/water.png"
import styles from "./card.styles"


const energyTypes = {
  "colorless": colorless, "darkness": darkness,
  "dragon": dragon, "fairy": fairy, "fighting": fighting,
  "fire": fire, "grass": grass, "lightning": lightning,
  "metal": metal, "other": other, "psychic": psychic, "water": water
}

// sets image to regular pokeball
let imgUrl = energyTypes["other"];

/**
 * Displays the information for an 'Energy' card
 * such as name, type, set, rarity,
 * flavour text, artist, index
 * @param {*} card (props)
 * @returns React component of a card to render
 */
const EnergyCard = ({ card }) => {

  // set image based on energy type
  for (const energy in energyTypes) {
    if (card.cardname.toLowerCase().includes(energy)) {
      imgUrl = energyTypes[energy];
    }
  }

  // there is an issue if the card length contain place holders for symbols
  // ex: 'Blend Energy WaterLightningFightingMetal'
  // need the substring of 'Blend Energy' since we do not care about the symbol names  
  const energy = "Energy";
  const cardname = card.cardname.substring(0, card.cardname.indexOf(energy) + energy.length)
  return (
    <Box maxWidth="350px" >
      <Grid
        color={"white"}
        margin={0}
        elevation={2}
        columns={6}
        container
        direction="row"
        alignItems="center"
        justifyContent="center"
        py={"20px"}
        paddingTop={0}
        paddingBottom={0}
        height={"400px"}
        style={{ backgroundColor: "#2b2b2b" }}
      >
        <Grid item xs={6}>
          <Paper elevation={0} sx={{ backgroundColor: "#3f3f3f", color: "#FFFFFF" }}>
            <Tooltip arrow title={<h2>{cardname}</h2>}>
              <Typography padding={"6px"} variant="h4" sx={styles.cardname}>{cardname}</Typography>
            </Tooltip>
          </Paper>
        </Grid>
        <Grid item xs={5}>
          <Paper sx={{ backgroundColor: "#12658e", color: "#FFFFFF" }}>
            <Typography variant="h6">
              {card.cardtype} {card.subtype}
            </Typography>
          </Paper>
        </Grid>
        <Grid item xs={5}>
          <Typography style={styles.mdHeading}>
            Set{" "}
          </Typography>
          <Typography style={{ display: "inline", fontSize: "20px" }}>{card.cardset.setname}{ }</Typography>
        </Grid>
        <Grid item xs={5}>
          <Typography style={styles.mdHeading}>
            Rarity{" "}
          </Typography>
          <Typography style={{ display: "inline", fontSize: "20px" }}>{card.rarity}</Typography>
        </Grid>
        {/* check if card contains flavor text or not for conditional rendering */}
        {card.flavortext === "No Card Text" &&
          <>
            <Grid item xs={4}>
              <img src={imgUrl} width="120px" alt="fire" />
            </Grid>
          </>
        }
        {card.flavortext !== "No Card Text" &&
          <>
            <Grid item xs={4}>
              <img src={imgUrl} width="100px" alt="fire" />
            </Grid>
            <Grid item xs={5}>
              <Box sx={{ maxHeight: "140px", overflow: 'auto' }}>
                <Typography
                  style={styles.description}
                  paragraph={true}
                >
                  {card.flavortext}
                </Typography>
              </Box>
            </Grid>
          </>
        }

        <Grid
          item
          xs={6}
          style={{ display: "flex", justifyContent: "flex-start" }}
        >
          <Box pl={"10px"}>
            <Typography style={{ display: "inline", fontWeight: "bold" }}>
              Artist{" "}
            </Typography>
            <Typography display="inline">{card.artist}</Typography>
          </Box>
        </Grid>
      </Grid>
    </Box>
  );
};

export default EnergyCard;
