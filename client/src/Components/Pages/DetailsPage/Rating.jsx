import React, { useState, useEffect } from "react";
import CatchingPokemonIcon from '@mui/icons-material/CatchingPokemon';
import StyledRating from "./rating.styles";
import { useKeycloak } from "@react-keycloak/web";

/**
 * Renders a react component that displays the rating of a card
 * @param {*} props
 * @returns React component to render
 */
export default function Rating(props) {
    const { cardid } = props;
    // get all comments for current card 
    const [ratings, setRating] = useState(0);
    // get the average of ratings
    const [average, setAverage] = useState(0);
    // initialize the keycloak object
    const { keycloak } = useKeycloak();

    /**
     * Fetches all the ratings on the card
     */
    const fetchRatings = async () => {
        let ratingsUrl = `/api/cards/${cardid}/rating`;
        const response = await fetch(ratingsUrl);
        if (response.ok) {
            const ratings = await response.json();
            setRating(ratings);
        }
    };

    // fetch all the ratings
    useEffect(() => {
        fetchRatings();
    }, []);

    // get the average ratings
    useEffect(() => {
        if (ratings !== undefined && ratings !== 0) {
            getAverage();
        }
    }, [ratings])

    /**
     * Calculates the average rating for all ratings on the given card
     */
    const getAverage = () => {
        let sum = 0;
        ratings.forEach((element, index, array) => {
            sum += Number(element.rating);
        });
        // set state for average
        setAverage(Number.parseFloat(sum / ratings.length).toFixed(2)); // rounds x.yyy.. -> x.y
    };

    // updates the ratings if user adds to it
    const addRating = async (value) => {
        // cards/base1-7/rating/1?userAuth=true
        let ratingUrl = `/api/cards/${cardid}/rating?userAuth=${keycloak.authenticated}&rating=${value}`;
        const request = await fetch(ratingUrl, {
            method: "POST",
            headers: { "Content-Type": "application/json" }
        });
        if (request.ok) {
            // update the ratings
            fetchRatings();
            getAverage();
        }
    }

    return (
        <div>
            <StyledRating
                precision={0.25}
                name="card-rating"
                defaultValue={0}
                value={Number(average)}
                onChange={(event, newValue) => {
                    addRating(newValue);
                }}
                getLabelText={(value) => `${value} Pokeball${value !== 1 ? 's' : ''}`}
                icon={<CatchingPokemonIcon fontSize="inherit" />}
                emptyIcon={<CatchingPokemonIcon sx={{ color: "#2d3a40" }} fontSize="inherit" />}
            />
        </div>
    );
}
