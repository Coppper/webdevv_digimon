import React, { useState } from "react";
import Avatar from "@mui/material/Avatar";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemAvatar from "@mui/material/ListItemAvatar";
import ListItemText from "@mui/material/ListItemText";
import DialogTitle from "@mui/material/DialogTitle";
import Dialog from "@mui/material/Dialog";
import AddIcon from "@mui/icons-material/Add";
import { TextField } from "@mui/material";
import CatchingPokemonRoundedIcon from "@mui/icons-material/CatchingPokemonRounded";
import { fetchCollectionsFromUser } from "../../../Components/Pages/Controls/CollectionsControl";
import { useEffect, useContext } from "react";
import SnackbarAlert from "../SnackbarAlert";
import { SnackbarContext } from "../../../context/SnackbarContext";

/**
 * Renders a react component that displays a dialog
 * which allows the user to add a card to collection
 * and add a new collection
 * @param {*} props
 * @returns React component to render
 */
export default function AddToCollectionDialog(props) {
  const { onClose, selectedValue, open, cardid, cardname, keycloak } = props;
  const [textValue, setTextValue] = useState("");

  // get all collections for current user (by username)
  const [collections, setCollections] = useState([]);
  const getCollections = async () => {
    // need to get the username from keycloak obj
    const collections = await fetchCollectionsFromUser(
      keycloak.idTokenParsed.preferred_username
    );
    setCollections(collections);
  };
  useEffect(() => {
    getCollections();
  }, []);

  // close the dialog
  const handleClose = () => {
    onClose(selectedValue);
  };

  /**
   * Adds a card to a collection
   * @param {json} collection to add to
   */
  const handleListItemClick = async (collection) => {
    let addCardUrl = `/api/collections/cards/${cardid}?userAuth=${keycloak.authenticated}&collectionid=${collection.collectionid}`;
    const response = await fetch(addCardUrl, { method: "POST" });
    if (!response.ok) {
      const errMessage = `An error has occured adding '${cardid}', ${cardname} to collection ${collection.collectionname}`;
      // snackbar error alert
      setSnackbar(errMessage, "error");
    }
    const successMessage = `Successfully added '${cardid}', ${cardname} to collection ${collection.collectionname}`;
    // snackbar success alert
    setSnackbar(successMessage, "success");
    onClose(collection.collectionid);
  };
  // set state of the TextField text
  const handleTextInput = (e) => {
    setTextValue(e.target.value);
  };

  const { setSnackbar } = useContext(SnackbarContext);

  /**
   * Adds a new collection for the currently logged in user
   * @param {*} e
   */
  const handleSubmit = async (e) => {
    let addCollectionUrl = `/api/collections?userAuth=${keycloak.authenticated}&user=${keycloak.idTokenParsed.preferred_username}`;
    let collectionJson = {
      collectionname: textValue,
    };
    let request;
    if (textValue != "") {
      request = await fetch(addCollectionUrl, {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(collectionJson),
      });
      if (request.status === 500) {
        const existsMessage = `Collection ${textValue} already exists`;
        setSnackbar(existsMessage, "error");
      } else {
        const successMessage = `Successfully added new collection ${textValue}`;
        // snackbar success alert
        setSnackbar(successMessage, "success");
      }
    }
    // reset state of the textfield
    setTextValue("");
    // fetch collections again
    getCollections();
  };

  /**
   * Enables submit on enter key
   * @param {*} e
   */
  const handleEnterKey = (e) => {
    // 13 = enter key
    if (e.keyCode === 13) {
      handleSubmit(e);
    }
  };

  return (
    <div>
      <SnackbarAlert />
      <Dialog
        maxWidth={"xs"}
        fullWidth={true}
        onClose={handleClose}
        open={open}
      >
        <DialogTitle>Add card to collection</DialogTitle>
        <List sx={{ pt: 0 }}>
          {/* we're mapping an object here, not an array */}
          {Object.values(collections).map((collection) => (
            <ListItem
              button
              onClick={() => handleListItemClick(collection)}
              key={collection.collectionid}
            >
              <ListItemAvatar>
                <Avatar sx={{ bgcolor: "#2d3a40" }}>
                  <CatchingPokemonRoundedIcon sx={{ color: "#af4b4d" }} />
                </Avatar>
              </ListItemAvatar>
              <ListItemText primary={collection.collectionname} />
            </ListItem>
          ))}
          <ListItem autoFocus button>
            <ListItemAvatar onClick={handleSubmit}>
              <Avatar sx={{ bgcolor: "#2d3a40" }}>
                <AddIcon sx={{ color: "#af4b4d" }} />
              </Avatar>
            </ListItemAvatar>
            <TextField
              value={textValue}
              id="standard-input"
              label="Add new collection"
              type="add-collection"
              variant="standard"
              onChange={handleTextInput}
              onKeyDown={handleEnterKey}
            />
          </ListItem>
        </List>
      </Dialog>
    </div>
  );
}
