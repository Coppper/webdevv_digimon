const styles = {
    lgHeading: {
        display: "inline", fontWeight: "bold", fontSize: "24px", letterSpacing: 2
    },
    text: {
        display: "inline", fontSize: "24px"
    },
    description: {
        margin: "0px",
        fontWeight: "400",
        backgroundColor: "#3b3b3b",
        padding: "12px"
    },
    cardname: {
        whiteSpace: "nowrap",
        overflow: "hidden",
        textOverflow: "ellipsis",
    },
    evolvesSmHeading: {
        display: "inline", fontWeight: "200", fontStyle: 'italic', whiteSpace: "nowrap",
        overflow: "hidden",
        textOverflow: "ellipsis",
    },
    evolvesLgHeading: {
        fontWeight: "600", whiteSpace: "nowrap",
        overflow: "hidden",
        textOverflow: "ellipsis",
    },
    mdHeading: {
        display: "inline", fontWeight: "bold", fontSize: "20px", letterSpacing: 1
    },
    longgHeading: {
        display: "inline", fontWeight: "bold", fontSize: "24px", letterSpacing: 2
    },
};

export default styles