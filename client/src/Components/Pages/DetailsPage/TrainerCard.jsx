import React, { } from "react";
import { Grid, Box, Paper, Typography, Tooltip } from "@mui/material";
import styles from "./card.styles"

/**
 * Displays the information for an 'Trainer' card
 * such as name, type, set, rarity
 * flavour text, artist, index
 * @param {*} card (props)
 * @returns React component of a card to render
 */
const TrainerCard = ({ card }) => {

  return (
    <Box maxWidth="350px" >
      <Grid
        color={"white"}
        margin={0}
        elevation={2}
        columns={6}
        container
        direction="row"
        alignItems="center"
        justifyContent="center"
        py={"20px"}
        paddingTop={0}
        paddingBottom={0}
        height={"400px"}
        style={{ backgroundColor: "#2b2b2b" }}
      >
        <Grid item xs={6}>
          <Paper elevation={0} sx={{ backgroundColor: "#3f3f3f", color: "#FFFFFF" }}>
            <Tooltip arrow title={<h2>{card.cardname}</h2>}>
            <Typography padding={"6px"} variant="h4" sx={styles.cardname}>{card.cardname}</Typography>
            </Tooltip>
          </Paper>
        </Grid>
        <Grid item xs={5}>
          <Paper sx={{ backgroundColor: "#12658e", color: "#FFFFFF" }}>
            <Typography variant="h6">
              {card.cardtype} {card.subtype}
            </Typography>
          </Paper>
        </Grid>
        <Grid item xs={5}>
          <Typography style={styles.lgHeading}>
            Set{" "}
          </Typography>
          <Typography style={styles.text}>{card.cardset.setname}</Typography>
          </Grid>
          <Grid item xs={5}>  
          <Typography style={styles.lgHeading}>
            Rarity{" "}
          </Typography>
          <Typography style={styles.text}>{card.rarity}</Typography>
        </Grid>
        <Grid item xs={5}>
          <Box sx={{maxHeight: "190px", overflow: 'auto' }}>
          <Typography
            style={styles.description}
            paragraph={true}
          >
            {card.flavortext}
          </Typography>
          </Box>
        </Grid>
        <Grid
          item
          xs={6}
          style={{ display: "flex", justifyContent: "flex-start" }}
        >
          <Box pl={"10px"}>
            <Typography style={{ display: "inline", fontWeight: "bold" }}>
              Artist{" "}
            </Typography>
            <Typography display="inline">{card.artist}</Typography>
          </Box>
        </Grid>
        <Grid
          item
          xs={2}
          style={{ display: "flex", justifyContent: "flex-end" }}
        >
        </Grid>
      </Grid>
    </Box>
  );
};

export default TrainerCard;
