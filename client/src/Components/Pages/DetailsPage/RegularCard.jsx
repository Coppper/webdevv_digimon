import React, { } from "react";
import { Grid, Box, Paper, Typography, Tooltip } from "@mui/material";
import evolve from "../../../img/evolve.png";
import styles from "./card.styles"

/**
 * Displays the information for an 'Regular' card
 * such as name, type, set, rarity, evoles to/from
 * flavour text, artist, index
 * @param {*} card (props)
 * @returns React component of a card to render
 */
const RegularCard = ({ card }) => {
  return (
    <Box maxWidth="350px" >
      <Grid
        color={"white"}
        margin={0}
        elevation={2}
        columns={6}
        container
        direction="row"
        alignItems="center"
        justifyContent="center"
        py={"20px"}
        paddingTop={0}
        paddingBottom={0}
        height={"400px"}
        style={{ backgroundColor: "#2b2b2b" }}
      >
        <Grid item xs={6}>
          <Paper elevation={0} sx={{ backgroundColor: "#3f3f3f", color: "#FFFFFF" }}>
            <Tooltip arrow title={<h2>{card.cardname}</h2>}>
              <Typography padding={"6px"} variant="h4" sx={styles.cardname}>{card.cardname}</Typography>
            </Tooltip>
          </Paper>
        </Grid>
        <Grid item xs={5}>
          <Paper sx={{ backgroundColor: "#12658e", color: "#FFFFFF" }}>
            <Typography variant="h6">
              {card.cardtype} {card.subtype}
            </Typography>
          </Paper>
        </Grid>
        <Grid item xs={5}>
          <Typography style={styles.mdHeading}>
            Set{" "}
          </Typography>
          <Typography style={{ display: "inline", fontSize: "20px" }}>{card.cardset.setname}{ }</Typography>
        </Grid>
        <Grid>
          <Typography style={styles.mdHeading}>
            Rarity{" "}
          </Typography>
          <Typography style={{ display: "inline", fontSize: "20px" }}>{card.rarity}</Typography>
        </Grid>
        <Grid
          columns={6}
          container
          spacing={0}
          style={{ backgroundColor: "#2b2b2b" }}>
          <Grid item xs={3}>
            <Typography variant={"caption"} style={styles.evolvesSmHeading}>
              evolves from{" "}
            </Typography>
          </Grid>
          <Grid item xs={3}>
            <Typography variant={"caption"} style={styles.evolvesSmHeading}>evolves to </Typography>
          </Grid>
          <Grid item xs={6}>
            <img src={evolve} width="50px" alt="evolves" />
          </Grid>
          <Grid item xs={3}>
            <Typography style={styles.evolvesLgHeading}>
              {card.evolvesfrom}
            </Typography>
          </Grid>
          <Grid item xs={3}>
            <Tooltip pr={10} arrow title={card.evolvesto}>
            <Typography style={styles.evolvesLgHeading}>
              {card.evolvesto}
            </Typography>
            </Tooltip>
          </Grid>
        </Grid>
        {/* check if card contains flavor text or not */}
        {card.flavortext !== "No Card Text" &&
          <Grid item xs={5} sx={{ maxHeight: "160px", overflow: 'auto' }}>
            <Typography
              style={styles.description}
              paragraph={true}
            >
              {card.flavortext}
            </Typography>
          </Grid>
        }
        <Grid
          item
          xs={4}
          style={{ display: "flex", justifyContent: "flex-start" }}
        >
          <Box pl={"10px"}>
            <Typography style={{ display: "inline", fontWeight: "bold" }}>
              Artist{" "}
            </Typography>
            <Typography display="inline">{card.artist}</Typography>
          </Box>
        </Grid>
        <Grid
          item
          xs={2}
          style={{ display: "flex", justifyContent: "flex-end" }}
        >
          <Box pr={"10px"}>
            <Typography style={{ display: "inline", fontWeight: "bold" }}>
              Index #{" "}
            </Typography>
            <Typography display="inline">{card.nationaldex}</Typography>
          </Box>
        </Grid>
      </Grid>
    </Box>
  );
};

export default RegularCard;
