import Rating from '@mui/material/Rating';
import { styled } from '@mui/material/styles';

const StyledRating = styled(Rating)({
    '& .MuiRating-iconFilled': {
      color: '#af4b4d',
    },
    '& .MuiRating-iconHover': {
      color: '#f07b7d',
    },
  });

export default StyledRating