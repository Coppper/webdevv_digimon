import React, { useState } from "react";
import {
  List,
  ListItemText,
  Avatar,
  ListItemAvatar,
  ListItem,
} from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import { TextField } from "@mui/material";
import CatchingPokemonRoundedIcon from "@mui/icons-material/CatchingPokemonRounded";
import { useEffect, useContext } from "react";
import SnackbarAlert from "../SnackbarAlert";
import { SnackbarContext } from "../../../context/SnackbarContext";
import { getUserIdFromUsername } from "../Controls/CollectionsControl";
import { Paper, Grid } from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import IconButton from "@mui/material/IconButton";
import { Link } from "react-router-dom";
import { useKeycloak } from "@react-keycloak/web";

/**
 * Renders a rReact component that displays a list of comments
 * which also allows the user to add a comment
 * users can delete their own comments
 * moderators can delete any comment
 * @param {*} props
 * @returns React component to render
 */
export default function Comments(props) {
  const { cardid, cardname } = props;
  const [textValue, setTextValue] = useState("");
  // get all comments for current card
  const [comments, setComments] = useState([]);
  // get the current user logged in
  const [userid, setUserid] = useState(null);
  // set moderator
  const [isModerator, setMod] = useState(false);
  // initialize the keycloak object
  const { keycloak } = useKeycloak();

  useEffect(() => {
    getUserid();
  }, []);

  useEffect(() => {
    getMod();
  }, []);
  const getUserid = async () => {
    if (keycloak.authenticated) {
      let userid = await getUserIdFromUsername(
        keycloak.idTokenParsed.preferred_username
      );
      setUserid(userid);
    }
  };

  getUserid();
  /**
   * Checks if the user is a moderator
   */
  const getMod = () => {
    try {
      if (
        keycloak.tokenParsed.realm_access.roles.includes("Moderator") &&
        keycloak != undefined
      ) {
        setMod(true);
      }
    } catch (TypeError) { }
  };

  /**
   * Fetches all the comments on the card
   */
  const fetchComments = async () => {
    let commentsUrl = `/api/cards/${cardid}/comments`;
    const response = await fetch(commentsUrl);
    if (response.ok) {
      const comments = await response.json();
      setComments(comments);
    }
  };

  // fetch all the comments
  useEffect(() => {
    fetchComments();
  }, []);

  /**
   *  Post request to add comment on card
   */
  const addComment = async () => {
    let commentJson = {
      content: textValue,
    };
    // cards/base1-7/comment/1?userAuth=true
    let addCommentUrl = `/api/cards/${cardid}/comment/${userid}?userAuth=${keycloak.authenticated}`;
    const request = await fetch(addCommentUrl, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(commentJson),
    });
    if (!request.ok) {
      const errMessage = `An unexpected error occured, your comment was not added`;
      // snackbar error alert
      setSnackbar(errMessage, "error");
    }
    const successMessage = `Successfully added comment on '${cardname}'`;
    // snackbar success alert
    setSnackbar(successMessage, "success");
    // reset state of the textfield
    setTextValue("");
    // update the comments
    fetchComments();
  };

  /**
   * Deletes a comment on a card
   * @param {int} comment (id) of the comment to delete
   */
  const deleteComment = async (comment) => {
    // cards/base1-6/comment/11?userAuth=true&delete=true&commentid=56
    let deleteCommentUrl = `/api/cards/${comment.cardid}/comment/${comment.user.userid}?userAuth=${keycloak.authenticated}&delete=true&commentid=${comment.commentid}`;
    const request = await fetch(deleteCommentUrl, {
      method: "DELETE",
      headers: { "Content-Type": "application/json" },
    });
    if (!request.ok) {
      const errMessage = `An unexpected error occured, your comment was not deleted`;
      // snackbar error alert
      setSnackbar(errMessage, "error");
    }
    const successMessage = `Successfully deleted comment`;
    // snackbar success alert
    setSnackbar(successMessage, "success");
    // update the comments
    fetchComments();
  };

  /**
   * Set state of the TextField text
   * @param {*} e 
   */
  const handleTextInput = (e) => {
    setTextValue(e.target.value);
  };

  const { setSnackbar } = useContext(SnackbarContext);

  /**
   * Adds a new comment for the card
   * @param {*} e
   */
  const handleSubmit = async (e) => {
    addComment();
  };

  /**
   * Enables submit on enter key
   * @param {*} e
   */
  const handleEnterKey = (e) => {
    // 13 = enter key
    if (e.keyCode === 13) {
      handleSubmit(e);
    }
  };

  return (
    <div>
      <SnackbarAlert />
      <Grid mt={"2vh"} mb={"2vh"}>
        <Paper
          mt={"2vh"}
          elevation={3}
          sx={{ maxHeight: "400px", overflow: "auto" }}
        >
          <List sx={{ mb: "4vh" }}>
            {/* Input field for user to add comment */}
            <ListItem autoFocus button>
              <ListItemAvatar onClick={handleSubmit}>
                <Avatar sx={{ bgcolor: "#2d3a40" }}>
                  <AddIcon sx={{ color: "#af4b4d" }} />
                </Avatar>
              </ListItemAvatar>
              <TextField
                fullWidth
                value={textValue}
                id="standard-input"
                label="Add comment"
                type="add-collection"
                variant="standard"
                onChange={handleTextInput}
                onKeyDown={handleEnterKey}
              />
            </ListItem>
            {/* map out the array of comments */}
            {comments.map((comment) => (
              <ListItem
                // go to profile of user that commented
                key={comment.commentid}
                secondaryAction={
                  <IconButton
                    edge="end"
                    aria-label="delete"
                    onClick={() => deleteComment(comment)}
                  >
                    {(isModerator || comment.user.userid === userid) && (
                      <DeleteIcon sx={{ color: "white", fontSize: "1.8rem" }} />
                    )}
                  </IconButton>
                }
              >
                {/* link avatar to users profile page */}
                <Link
                  target="_blank"
                  key={comment.user.userid}
                  to={`/profile/${comment.user.username}`}
                >
                  <ListItemAvatar>
                    <Avatar sx={{ bgcolor: "#2d3a40" }}>
                      <CatchingPokemonRoundedIcon sx={{ color: "#af4b4d" }} />
                    </Avatar>
                  </ListItemAvatar>
                </Link>
                <ListItemText
                  sx={{ display: "flex", flexDirection: "column-reverse" }}
                  primary={comment.content}
                  secondary={
                    comment.user.username
                      ? comment.user.username
                      : "no username"
                  }
                />
              </ListItem>
            ))}
          </List>
        </Paper>
      </Grid>
    </div>
  );
}
