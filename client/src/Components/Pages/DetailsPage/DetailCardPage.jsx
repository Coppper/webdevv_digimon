import React, { useState } from "react";
import { useParams } from "react-router-dom";
import { Paper, Grid } from "@mui/material";
import styles from "./DetailCardPage.module.css";
import ImageFetch from "../../../Helpers/imageFetch";
import { useEffect } from "react";
import RegularCard from "./RegularCard";
import TrainerCard from "./TrainerCard";
import EnergyCard from "./EnergyCard";
import AddToCollectionDialog from "./AddToCollectionDialog"
import BookmarkAddIcon from '@mui/icons-material/BookmarkAdd';
import { useKeycloak } from "@react-keycloak/web";
import Box from '@mui/material/Box';
import loadingIcon from "../../../img/pokeballicon.png"
import "../../../styles/loadingIcon.css"
import CommentIcon from '@mui/icons-material/Comment';
import IconButton from '@mui/material/IconButton';
import Comments from "./Comments";
import Zoom from '@mui/material/Zoom';
import Rating from "./Rating"



function map(val, minA, maxA, minB, maxB) {
  return minB + ((val - minA) * (maxB - minB)) / (maxA - minA);
}

/**
 * React componenet that displays the details of a Pokemon card
 * @returns React component to render
 */
export default function DetailCardHook() {
  let params = useParams();
  const { keycloak } = useKeycloak();
  const [imgUrl, setImg] = useState("https://pokemonblobs.blob.core.windows.net/images/undefined/undefined");
  const [retrieved, setCard] = useState({});
  useEffect(() => {
    getCard().then(card => checkURL(card.setid, card.cardid.split("-")[1]));
  }, []);

  const url = `/api/cards/${params.id}`;
  /**
   * Sets the card based on the params
   * @returns 
   */
  const getCard = async () => {
    const response = await fetch(url);
    const data = await response.json();
    setCard(data.card);
    return data.card;
  };

  /**
   * Gets the image from the database to set card image
   * @param {int} setID 
   * @param {int} cardID 
   */
  async function checkURL(setID, cardID) {
    var imgUrl = `https://pokemonblobs.blob.core.windows.net/images/${setID}/${cardID}`;
    var http = new XMLHttpRequest();
    http.open("HEAD", imgUrl, false);
    http.setRequestHeader("Access-Control-Allow-Origin", "*");
    http.send();
    if (http.status !== 200) {
      // page did not load
      ImageFetch(setID, cardID).then(apiURL => setImg(apiURL));
    } else {
      setImg(imgUrl);
    }
  }
  /**
   * Set default state for the card (prior to any animation effects)
   */
  const [CardState, setCardState] = useState({
    xRotation: 0,
    yRotation: 0,
    brightness: 1,
  });
  /**
   * Handles the over effect when viewing a card
   * @param {*} e 
   */
  function handleMouseHover(e) {
    let mouseX = e.nativeEvent.offsetX;
    let mouseY = e.nativeEvent.offsetY;
    let yRotation = map(mouseX, 0, 256, -25, 25);
    let xRotation = map(mouseY, 0, 355, 25, -25);
    let brightness = map(mouseY, 0, 250, 1.2, 0.8);

    setCardState({
      xRotation,
      yRotation,
      brightness,
      scale: 1.3,
    });
  }

  /**
   * Resets the card state when no mouse on hover
   */
  function handleMouseLeave() {
    setCardState({
      xRotation: 0,
      yRotation: 0,
      brightness: 1,
      scale: 1,
    });
  }


  // state for the dialog being in open/closed state
  const [open, setOpen] = React.useState(false);
  // set the selected value of the collection
  const [selectedValue, setSelectedValue] = React.useState({});
  // state for the comments section
  const [expanded, setExpanded] = React.useState(false);

  /**
   * Handles the expanded state to be true or false (comments section)
   */
  const handleExpandClick = () => {
    setExpanded((previousOpen) => !previousOpen);
  };

  /**
   * Handles opening the 'add to collection' dialog
   * @param {*} e 
   */
  const handleClickOpen = (e) => {
    e.preventDefault();
    setOpen(true);
  };

  /**
   * Handles closing the 'add to collection' dialog
   * @param {*} value 
   */
  const handleClose = (value) => {
    setOpen(false);
    setSelectedValue(value);
  };

  // comment element (needed for Zoom component)
  const commentElement = (
    <Comments
      cardid={retrieved.cardid} // the card we want to add
      cardname={retrieved.cardname}
      keycloak={keycloak}>
    </Comments>
  );

  // display a loading icon before the card is retrieved
  if (Object.keys(retrieved).length === 0 && imgUrl) {
    return (
      // loading pokeball icon
      <Box sx={{ display: 'flex', mt: "20vh" }}>
        <img
          alt="loading icon"
          src={loadingIcon}
          className="pokeball"
          width="60"
          height="60" />
      </Box>
    );
  }

  return (
    <Grid
      direction="column"
      container
      alignItems="center">

      <Grid
        direction="row"
        alignContent="center"
        justifyContent="space-evenly"
        alignItems="flex-start"
        flexWrap="nowrap"
        container
        mt={"50px"}
        ml={"auto"}
        mr={"auto"}
        width={"60%"}
      >
        <Grid
          width="auto"
          direction="column"
          alignContent="center"
          justifyContent="space-evenly"
          alignItems="center"
          flexWrap={"wrap"}
          spacing={0}
          container>
          <Paper
            className={styles.card}
            elevation={3}
            onMouseLeave={() => handleMouseLeave()}
            onMouseMove={(e) => handleMouseHover(e)}
            sx={{
              m: 0,
              width: 256,
              height: 355,
              borderRadius: "10px",
              transform: `rotateX(${CardState.xRotation}deg) 
                          rotateY(${CardState.yRotation}deg) scale(${CardState.scale})`,
              filter: `brightness(${CardState.brightness})`,
            }}
          >
            <img
              alt="card"
              src={`${imgUrl}`}
              style={{ width: "100%", height: "auto" }}
            />
          </Paper>
          {/* only display options to add rate, add card, comment if user logged in */}
          {keycloak.authenticated && (
            <>
              <AddToCollectionDialog
                selectedValue={selectedValue} // the selected collection to add to
                cardid={retrieved.cardid} // the card we want to add
                cardname={retrieved.cardname}
                open={open}
                onClose={handleClose}
                keycloak={keycloak}
              />
              {/* displays rating, add card, comment */}
              <Paper elevation={2} sx={{ minWidth: 256, maxWidth: 256, mt: 1 }}>
                <Grid
                  id="options-box"
                  pl="32px"
                  pr="32px"
                  columns={6}
                  container
                  direction="row"
                  alignItems="center"
                  justifyContent="space-evenly"
                >
                  {/* Rating pokeballs */}
                  <Grid
                    container
                    justifyContent="space-evenly"
                    item xs={3}>
                    <Rating cardid={retrieved.cardid} />
                  </Grid>
                  {/* Add to collection button */}
                  <Grid item xs={1}>
                    <IconButton
                      color="secondary"
                      type="submit"
                      variant="contained"
                      onClick={handleClickOpen} >
                      <BookmarkAddIcon sx={{ fontSize: "1.6rem" }} />
                    </IconButton>
                  </Grid>
                  {/* Comment button/icon */}
                  <Grid item xs={1}>
                    <IconButton
                      color="primary"
                      type="submit"
                      onClick={handleExpandClick} >
                      <CommentIcon sx={{ fontSize: "1.6rem" }} />
                    </IconButton>
                  </Grid>
                </Grid>
              </Paper>
            </>
          )}
        </Grid>
        {/* conditional rendering based on the card type */}
        <Box id="card-desc">
          {retrieved.cardtype === "Pokémon" && <RegularCard card={retrieved}> </RegularCard>}
          {retrieved.cardtype === "Trainer" && <TrainerCard card={retrieved}> </TrainerCard>}
          {retrieved.cardtype === "Energy" && <EnergyCard card={retrieved}> </EnergyCard>}
        </Box>
      </Grid>
      {/* Displays all the comments for the card */}
      <Box width={"46%"}>
        <Zoom in={expanded}>
          <Box>
            {commentElement}
          </Box>
        </Zoom>
      </Box>
    </Grid>
  );
}
