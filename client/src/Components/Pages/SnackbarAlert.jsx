
import React, { useContext } from 'react';
import Snackbar from "@mui/material/Snackbar";
import Alert from "@mui/material/Alert"
import { SnackbarContext } from "../../context/SnackbarContext";

/**
 * Component to display a MUI Snackbar
 * @param {*} props 
 * @returns MUI Snackbar with Alert
 */
export default function SnackbarAlert(props) {

  const { snackbar, setSnackbar } = useContext(SnackbarContext);
  const { message, severity } = snackbar;

  const handleAlertClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    // since it is null it will not display
    setSnackbar(null);
  };

  const vertical = "bottom";
  const horizontal = "center"

  return (
    <div>
      <Snackbar
        anchorOrigin={{ vertical, horizontal }}
        onClose={handleAlertClose}
        autoHideDuration={4000}
        // display only if there is a message
        open={!!message}
        message={message ? <span>{message}</span> : null}
      >
        <Alert open={!!message} onClose={handleAlertClose} severity={severity}>
          {message}
        </Alert>
      </Snackbar>
    </div>
  );
}