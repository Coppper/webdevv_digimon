import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { Container, Box, Typography, IconButton, Menu, MenuItem, Paper } from "@mui/material";
import EditModal from "./EditModal.jsx"
import { useKeycloak } from "@react-keycloak/web";
import { fetchCollectionsFromUser } from "../Controls/CollectionsControl";

import LocationOnIcon from '@mui/icons-material/LocationOn';
import CalendarTodayIcon from '@mui/icons-material/CalendarToday';
import ShieldIcon from '@mui/icons-material/Shield';
import MoreHorizIcon from '@mui/icons-material/MoreHoriz';
import FavoriteIcon from '@mui/icons-material/Favorite';
import PeopleIcon from '@mui/icons-material/People';

import ProfileList from "./ProfileList.jsx"
import pokemonbackground from "../../../img/pokemonbackground.png"
import { CardMedia } from "@mui/material";

export default function HomePage() {
  let { username } = useParams();

  const { keycloak, initialized } = useKeycloak();
  let keycloakUsername = keycloak.idTokenParsed?.preferred_username;

  const [loggedInUser, setLoggedInUser] = useState({});
  const [profileUser, setProfileUser] = useState({});
  const [friendStatus, setFriendStatus] = useState(0);
  const [followers, setFollowers] = useState([]);
  const [collections, setCollections] = useState([]);
  const [favorites, setFavorites] = useState([]);

  const [openEditModal, setOpenEditModal] = React.useState(false);
  const handleOpenEditModal = () => setOpenEditModal(true);
  const handleCloseEditModal = () => setOpenEditModal(false);

  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  const handleClick = (event) => { setAnchorEl(event.currentTarget) };
  const handleClose = () => { setAnchorEl(null) };

  useEffect(() => {
    getProfileUser();
    getLoggedInUser();
  }, [username, keycloakUsername]);

  useEffect(() => {
    checkFriendStatus();
  }, [loggedInUser]);

  useEffect(() => {
    getUserFriendList();
    fetchCollectionsFromUser(profileUser?.username).then((collections) => setCollections(collections));
    getFavorites();
  }, [profileUser]);

  const getProfileUser = async () => {
    if (!username) return;

    const url = `/api/users/username/${username}`;
    const response = await fetch(url);
    const data = await response.json();
    setProfileUser(data.user);

    return data.user;
  }

  const getLoggedInUser = async () => {
    if (!keycloakUsername) return;

    const url = `/api/users/username/${keycloakUsername}`;
    const response = await fetch(url);
    const data = await response.json();
    setLoggedInUser(data.user);

    return data.user;
  }

  const checkFriendStatus = async () => {
    if (!profileUser || Object.keys(profileUser).length === 0) return; //null and empty check to avoid unnecessary calls
    if (!loggedInUser || Object.keys(loggedInUser).length === 0) return; //null and empty check to avoid unnecessary calls

    const url = `/api/follow/${loggedInUser.userid}/${profileUser.userid}`;
    const response = await fetch(url);
    const data = response.status === 200;

    const url2 = `/api/follow/${profileUser.userid}/${loggedInUser.userid}`;
    const response2 = await fetch(url2);
    const data2 = response2.status === 200;

    setFriendStatus([data, data2].filter(Boolean).length);

    return data.follow;
  }

  const getUserFriendList = async () => {
    if (!profileUser || Object.keys(profileUser).length === 0) return; //null and empty check to avoid unnecessary calls

    const url = `/api/followers/${profileUser.userid}`;
    const response = await fetch(url);
    const data = await response.json();

    setFollowers(data.follow);
  }

  const getFavorites = async () => {
    if (!profileUser || Object.keys(profileUser).length === 0) return; //null and empty check to avoid unnecessary calls

    const url = `/api/collections/favorites/${profileUser.userid}`;
    const response = await fetch(url);
    const data = await response.json();


    setFavorites(data.cards);
  }

  const handleAddToFriends = async () => {
    await fetch(`/api/follow/${loggedInUser.userid}/${profileUser.userid}/`, { method: 'POST' });

    handleClose();
    setFriendStatus(friendStatus + 1);
    setFollowers(old => [...old, loggedInUser]);
  }

  const handleRemoveFriend = async () => {
    await fetch(`/api/follow/${loggedInUser.userid}/${profileUser.userid}/`, { method: 'DELETE' });

    let newFollowers = followers;
    newFollowers = newFollowers.filter(obj => obj.userid !== loggedInUser.userid);
    setFollowers(newFollowers);

    handleClose();
    setFriendStatus(friendStatus - 1);
  }

  const Moderator = () => {
    try {
      if (
        keycloak.tokenParsed.realm_access.roles.includes("Moderator") &&
        keycloak != undefined && profileUser.username === keycloak.idTokenParsed?.preferred_username
      ) {
        return (

          <Typography variant="p" component="p" sx={headerTypography}>
            <ShieldIcon sx={{ marginBottom: "-5px" }} /> Moderator
          </Typography>
        );
      } else {
        return <Box></Box>;
      }
    } catch (TypeError) {
      return <Box></Box>
    }
  };



  const containerCss = { marginTop: "-30px", zIndex: -10, position: "absolute", padding: "0 !important" }
  const paperStyle = { backgroundColor: "#274966a6 !important", width: "28%" }
  const paperHeader = { backgroundColor: "#526e85", p: "1vh", borderTopLeftRadius: "inherit", borderTopRightRadius: "inherit" }
  const headerTypography = { marginTop: "2vh", marginRight: 4 }

  return (
    <Container maxWidth="120vw" sx={containerCss}>
      <EditModal open={openEditModal} onClose={handleCloseEditModal} user={profileUser} setUser={setProfileUser} />
      <Box sx={{ bgcolor: '#1a1a1a', height: '180px' }} />
      <Box sx={{ bgcolor: '#3a1515', height: '160px', display: "flex", flexDirection: "row" }} >
        <CardMedia
          component="img"
          sx={{ width: 200, height: 200, marginTop: "-100px", marginLeft: 25 }}
          image={pokemonbackground}
          alt="profile avatar"
        />
        <Container sx={{ textAlign: "left", margin: "1vh 0 auto 0" }}>
          <Typography variant="h3" component="h3" color="#b0cedd">
            {profileUser.username}&nbsp;
            <IconButton id="basic-button" aria-controls={open ? 'basic-menu' : undefined} aria-haspopup="true" aria-expanded={open ? 'true' : undefined} onClick={handleClick} aria-label="delete" sx={{ backgroundColor: "#0d405a", color: "white", height: "40px", width: "40px" }}>
              <MoreHorizIcon fontSize="inherit" />
            </IconButton>
            <Menu
              id="basic-menu"
              anchorEl={anchorEl}
              open={open}
              onClose={handleClose}
              MenuListProps={{
                'aria-labelledby': 'basic-button',
              }}
            >
              {loggedInUser.username === profileUser.username && (
                <MenuItem onClick={() => { handleClose(); handleOpenEditModal(); }}>Edit profile</MenuItem>
              )}
              {loggedInUser.username !== profileUser.username && friendStatus === 0 &&
                <MenuItem onClick={handleAddToFriends}>Add to friends</MenuItem>
              }
              {loggedInUser.username !== profileUser.username && friendStatus !== 0 &&
                <MenuItem onClick={handleRemoveFriend}>Remove from friends</MenuItem>
              }
              {loggedInUser.username !== profileUser.username &&
                <MenuItem onClick={handleClose}>Report account</MenuItem>
              }
            </Menu>
          </Typography>
          <Typography variant="p" component="p" >{profileUser.bio}</Typography>
          <Container sx={{ display: "flex", flexDirection: "row", margin: 0, padding: "0 !important" }}>
            {profileUser.location !== null && profileUser.location !== ""  && (
              <Typography variant="p" component="p" sx={{ marginTop: "2vh", marginRight: 4 }}>
                <LocationOnIcon sx={{ marginBottom: "-5px" }} />{profileUser.location}
              </Typography>
            )}

            <Typography variant="p" component="p" sx={{ marginTop: "2vh", marginRight: 4 }}><CalendarTodayIcon sx={{ marginBottom: "-5px" }} /> User since 2/28/2022</Typography>

            {loggedInUser.username !== profileUser.username && friendStatus === 2 && (
              <Typography variant="p" component="p" sx={headerTypography}><FavoriteIcon sx={{ marginBottom: "-5px" }} /> Friend</Typography>
            )}

            {loggedInUser.username !== profileUser.username && friendStatus === 1 && (
              <Typography variant="p" component="p" sx={headerTypography}><PeopleIcon sx={{ marginBottom: "-5px" }} /> You follow</Typography>
            )}
            <Moderator />
          </Container>
        </Container>
      </Box>
      <Box sx={{ pb: '10vh', display: "flex", flexDirection: "row", mt: 7, placeContent: "center", px: 24, justifyContent: "space-around" }} >
        <Paper sx={paperStyle} elevation={3} >
          <Typography sx={paperHeader} variant="h6" component="div">Public Collections</Typography>
          <ProfileList items={collections} type="collection" />
        </Paper>
        <Paper sx={paperStyle} elevation={3} >
          <Typography sx={paperHeader} variant="h6" component="div">Favorite cards</Typography>
          <ProfileList items={favorites} type="favorite" />
        </Paper>
        <Paper sx={paperStyle} elevation={3} >
          <Typography sx={paperHeader} variant="h6" component="div">Followers</Typography>
          <ProfileList items={followers} type="follower" />
        </Paper>
      </Box>
    </Container>
  )
}