import React, { useState, useEffect } from "react";
import Modal from '@mui/material/Modal';
import TextField from '@mui/material/TextField';

import { Box, Typography, Button } from "@mui/material";

export default function EditModal(props) {
  const [location, setLocation] = useState(props.user.location);
  const [bio, setBio] = useState(props.user.bio);

  useEffect(() => {
    setLocation(props.user.location);
    setBio(props.user.bio);
  }, [props.user])

  const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4,
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    const url = `/api/users/${props.user.userid}`

    const body = { 
      "location": location,
      "bio": bio
    }

    const requestParams = {
      method: "POST",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(body)
    }

    const newUserProfile = props.user;
    newUserProfile.location = location;
    newUserProfile.bio = bio;

    props.setUser(newUserProfile);

    fetch(url, requestParams);
    props.onClose();
  }
  
  return (
    <Modal
      open={props.open}
      onClose={props.onClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box sx={style}>
        <form onSubmit={() => {}}>
          <Typography id="modal-modal-title" variant="h6" component="h2">Edit profile</Typography>
          <TextField 
            disabled={true} 
            id="outlined-basic" 
            label="Username" 
            variant="outlined" 
            defaultValue={props.user.username}
            sx={{margin: "20px 0"}} 
          />
          <TextField 
            id="outlined-basic" 
            label="Location" 
            variant="outlined" 
            defaultValue={props.user.location} 
            onChange={e => setLocation(e.target.value)} 
            sx={{margin: "20px 0"}}
          />
          <TextField
            id="outlined-multiline-static"
            label="Multiline"
            multiline
            rows={4}
            defaultValue={props.user.bio}
            onChange={e => setBio(e.target.value)}
            sx={{margin: "20px 0"}}
          />

          <Button type="submit" variant="contained" onClick={handleSubmit}>Update profile</Button>
        </form>
      </Box>
    </Modal>
  )
}
