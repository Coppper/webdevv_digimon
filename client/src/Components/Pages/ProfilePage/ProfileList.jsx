import React, { useState, useEffect } from "react";
import { Link } from 'react-router-dom';

import { List, ListItemButton, ListItemText, Avatar, ListItemAvatar } from "@mui/material";
import pokemonavatar from "../../../img/pokemonavatar.svg"
import { CardMedia } from "@mui/material";

export default function ProfileList(props) {
  const [listContent, setListContent] = useState([]);

  useEffect(async () => {
    if(!props.items || !props.type) return;

    if(props.type === "follower") generateProfileListObjectArray(await generateFriendsList(), props.type)
    else generateProfileListObjectArray(props.items, props.type);
  }, [props]);

  async function generateFriendsList() {
    const friendsList = [];

    for(const friend of props.items) {
      const id = friend.userid;

      const url = `/api/users/${id}`;
      const response = await fetch(url);
      const data = await response.json();

      friendsList.push({ "username": data.user.username });
    }

    return friendsList;
  }

  const generateProfileListObject = (obj, type) => {
    if(type === "collection")
      return { name: obj.collectionname, link: `/collection/${obj.collectionid}`, image: null };
    else if(type === "follower")
      return { name: obj.username, link: `/profile/${obj.username}`, image: "lol" }
    else if(type === "favorite")
      return { name: obj.cardname, link: `/card/${obj.cardid}`, image: null }
  }

  const generateProfileListObjectArray = (arr, type) => {
    const newArray = [];

    arr.forEach(element => {
      newArray.push(generateProfileListObject(element, type));
    });

    setListContent(newArray);
  }

  function generate() {
    if(listContent.length === 0) return <div></div>

    else return listContent.map((value) =>
      <Link key={value.name} to={value.link} style={{color: "white", textDecoration: "none"}}>
        <ListItemButton>
          { value.image !== null &&
            <ListItemAvatar sx={{marginRight: "10px"}}>
              <CardMedia
              width="50px"
              component="img"
              image={pokemonavatar}
              alt="profile avatar"
            />
            </ListItemAvatar>
          }
          <ListItemText primary={value.name} />
        </ListItemButton>
      </Link>
    );
  }

  return (
    <List sx={{ maxHeight: 350, overflow: 'auto' }}>
      {generate()}
    </List>
  )
}