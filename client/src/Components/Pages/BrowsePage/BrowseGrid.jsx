import * as React from 'react';
import { useParams } from "react-router-dom";
import { Grid, Pagination, Box, IconButton, Alert, AlertTitle, Collapse, Typography } from '@mui/material';
import CloseIcon from '@mui/icons-material/Close'
import FormLabel from '@mui/material/FormLabel';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import ImageFetch from "../../../Helpers/imageFetch.js";
import GridList from "../../GridComponents/GridList";
import { Checkbox } from '@mui/material';
import { useNavigate, useSearchParams, useLocation } from 'react-router-dom';
import loadingIcon from "../../../img/pokeballicon.png";
import "../../../styles/loadingIcon.css";


/**
 * This method sends an update request to our api to
 * update our database with the api link to limit
 * the amount of http requests we make
 * @param {*} set 
 * @param {*} id 
 */
function updateImage(set, id) {
  const imgId = set + "-" + id;
  const imgLink = { cardapilink: `https://pokemonblobs.blob.core.windows.net/images/${set}/${id}` };
  fetch(`/api/cards/upload/${imgId}`, {
    method: 'POST',
    mode: 'same-origin',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(imgLink),
  })
    .then(response => response.json())
    .then(data => {
    })
    .catch((error) => {
    });
}

/**
 * All pokemon types for the checkboxes
 */
const type = [
  "Darkness", "Metal", "Water",
  "Dragon", "Lightning", "Fire",
  "Fighting", "Colorless", "Grass",
  "Fairy", "Psychic", "No Type"
]

/**
 * Takes in parameters of the url and instianciate array of boolean
 * that represents which type are checked. Remembers what the user checked for the search.
 * @param {Query of types in url} params 
 * @returns 
 */
const setCheckbox = (params) => {
  let CheckBoxArr = []
  CheckBoxArr = type.map((v, i) => {
    return (0 <= params.indexOf(v))
  })
  return CheckBoxArr

}

export default function BrowseGrid() {
  let navigate = useNavigate();
  let params = useParams();
  let location = useLocation();
  const [alert, setAlert] = React.useState(false)
  const [tags, setTags] = useSearchParams()
  //save page in cache
  const [names, setNames] = React.useState([])
  const [isChecked, setIsChecked] = React.useState(setCheckbox(tags.getAll("t")))
  const [page, setPage] = React.useState(parseInt(params.page));
  const [carddata, setCardData] = React.useState([]);
  const [metadata, setMetaData] = React.useState(0);
  const [loading, setLoading] = React.useState(false)

  /**
  * Check the URL exists in the blob storage. If it does 
  * update the image url if not fetch the image using ImageFetch
  * 
  * @param {*} setID 
  * @param {*} cardID 
  * @returns imageurl
  */
  async function checkURL(setID, cardID) {
    var url = `https://pokemonblobs.blob.core.windows.net/images/${setID}/${cardID}`;
    var http = new XMLHttpRequest();
    http.open("HEAD", url, false);
    http.setRequestHeader("Access-Control-Allow-Origin", "*");
    http.send();
    if (http.status != 200) {
      // page did not load
      ImageFetch(setID, cardID).then((apiURL) => { url = apiURL });
      return url;
    } else {
      updateImage(setID, cardID);
      return url;
    }
  }

  /**
   * Loop through the array of carddata to check if it 
   * contains the apilink for the card images
   * 
   * @param {*} carddata 
   */
  function modifyData(carddata) {
    carddata.forEach(element => {
      if (element.cardapilink === null) {
        element.cardapilink = checkURL(element.setid, element.cardid.split("-")[1]);;
      }
    });
    setCardData(carddata);
  }

  /**
   * Handles 500 response of the json being fetched from the server
   * @param {JSON Response} response 
   * @returns JSON
   */
  const handleResponse = (response) => {
    if (response.status === 500) {
      setAlert(true)
      setLoading(false)
      return []
    }
    return response.json()
  }

  /**
   * Handles the pagination to fetch the next/previous page of cards.
   * @param {event of pagination} event 
   * @param {page number} value 
   */
  const handleChange = (event, value) => {
    setPage(value)
    setCardData([])
    navigate('/browse/' + value + '?' + tags.toString())
  }

  /**
   * adds and remove type parameters that needs to be filter in the URL
   * Updates the checkbox UI when parameters are updated.
   * @param {event value of checkbox} event 
   */
  const handleCheckbox = (event) => {
    //Updates UI
    setIsChecked((isChecked) => {
      return isChecked.map((c, i) => {
        if (i === type.indexOf(event.target.value)) return event.target.checked;
        return c;
      });
    });
    //Updates the URL.
    if (event.target.checked) {
      tags.append("t", event.target.value)  // adds filter value in parameter 't'
    }
    else {
      let newQuery = tags.getAll("t").filter((x) => { return x !== event.target.value })
      tags.delete("t")
      newQuery.forEach((x) => { tags.append("t", x) }) // resets the filter values to reflect the checkbox states in the URL
    }
    setPage(1)
    navigate('/browse/' + 1 + '?' + tags.toString())
  }

  function handleItemClick(id) {
    navigate("/card/" + id);
  }



  /**
   * Fetches the data from the server when either state
   * 'page' or 'tags' changes to emulate navigation
   */
  React.useEffect(() => {
    setLoading(true)
    if (Array.from(tags.values()).length > 0) {
      fetch('/api/cards/search/params?' + tags.toString() + "&page=" + (page - 1))
        .then(response => handleResponse(response))
        .then((data) => {
          setMetaData(data.metadata.total);
          setNames(data.metadata.tags)
          modifyData(data.paginate);
          setLoading(false)
        })
        .catch(
          function () { setLoading(false); }
        );
    }
    else {
      fetch('/api/cards/page/' + (page - 1))
        .then(response => handleResponse(response))
        .then((data) => {
          setMetaData(data.metadata.total);
          modifyData(data.paginate);
          setLoading(false)
        })
        .catch(
          function () { setLoading(false); }
        );
    }
  }, [page, tags]);

  /**
   * further emulation of navigation between URL
   */
  React.useEffect(() => {
    setNames([])
    setIsChecked(setCheckbox(tags.getAll("t")))
    setPage(parseInt(params.page))
  }, [location])

  /**
   * alert UI to make user aware that something went wrong 
   * when feteching the data from the server
   * @returns React components/HTML
   */
  const alertUI = () => {
    return (
      <Collapse in={alert}>
        <Alert
          severity="error"
          action={
            <IconButton
              aria-label="close"
              color="inherit"
              size="small"
              onClick={() => {
                setAlert(false);
              }}
            >
              <CloseIcon fontSize="inherit" />
            </IconButton>
          }
          sx={{ mb: 2 }}
        >
          <AlertTitle>Error</AlertTitle>
          Something went wrong!
        </Alert>
      </Collapse>
    )
  }

  /**
   * loading UI to hide cards from loading in chunks.
   * @returns React components/HTML
   */
  const loadingScreen = () => {
    return (
      // loading pokeball icon
      <Box sx={{ display: 'flex', mt: "20vh" }}>
        <img
          alt="loading icon"
          src={loadingIcon}
          className="pokeball"
          width="60"
          height="60" />
      </Box>
    );
  }

  /**
   * Shows the result of cards in a grid UI.
   * Consist of the browse UI page
   * @returns 
   */
  const showResult = () => {
    return (

      <div style={{ height: 650, width: '100%' }}>
        {alert ? alertUI() : <></>}
        {names.length > 0 ? <h1>Results for {names.map((x) => { return " '" + x + "'" })}</h1> :
          <></>}
        <Grid direction="row" flexWrap="nowrap" container spacing={1} columns={12}>
          <Grid item xs={10} md={10} lg={9} xl={9}>

            {carddata.length === 0 && (
              <Box>
                <Typography variant="h3"
                  sx={{ textAlign: "left" }}>
                  No Results Available
                </Typography>
              </Box>
            )}
            {carddata.length !== 0 && (
              <Box>

                <Pagination
                  count={Math.ceil(metadata / 30)}
                  page={page}
                  onChange={handleChange}
                  color="secondary"
                  sx={{ paddingBottom: 2 }}
                />

                <GridList cards={carddata} onClick={(id) => handleItemClick(id)} />

                <Pagination
                  count={Math.ceil(metadata / 30)}
                  page={page}
                  onChange={handleChange}
                  color="secondary"
                  sx={{ paddingTop: 2, paddingBottom: 2 }}
                />

              </Box>
            )}

          </Grid>
          <Grid item xs={2} md={2} lg={3} xl={2}>
            <h1>Filters</h1>
            <FormControl>
              <FormLabel id="demo-radio-buttons-group-label">Type</FormLabel>
              <RadioGroup
                name="radio-buttons-group"
              >
                {type.map((value, index) => {
                  return <FormControlLabel value={value} control={<Checkbox checked={isChecked[index]} onChange={handleCheckbox} />} label={value} />
                })}
              </RadioGroup>
            </FormControl>
          </Grid>
        </Grid>
      </div>
    );

  }

  return (
    <>
      {loading ? loadingScreen() : showResult()}
    </>
  );

}