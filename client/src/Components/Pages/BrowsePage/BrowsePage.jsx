import React from "react";
import ReactDOM from 'react-dom';
import BrowseGrid from "./BrowseGrid";
import { Box } from "@mui/material";

/**
 * Uses the Component of BrowseGrid to render a list of cards 
 */
class BrowsePage extends React.Component {
  render() {
    return (
      <Box sx={{ml: "10vh", my: "7vh"}}>
        <BrowseGrid />
      </Box>
    )
  }
}

export default BrowsePage;
