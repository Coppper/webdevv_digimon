import React, { useState, useEffect } from 'react';
import { Typography, List, ListItem, ListItemAvatar, ListItemText, Avatar, Divider, Skeleton } from '@mui/material';
import { flexbox } from '@mui/system';

export default function SliderCard(props) {
  return (
    <List sx={{ width: '100%', bgcolor: 'background.paper' }}>
      <ListItem alignItems="flex-start">
        <ListItemAvatar>
          <Skeleton variant="circular" width={40} height={40} />
        </ListItemAvatar>
        <ListItemText sx={{ display: "flex", flexDirection: "row" }}>
          <Skeleton variant="rectangular" width={150} height={20} sx={{ marginBottom: 2 }} />
          <Skeleton variant="rectangular" width={400} height={20} sx={{ marginBottom: 1 }} />
          <Skeleton variant="rectangular" width={300} height={20} />
        </ListItemText>
      </ListItem>

      <Divider variant="inset" component="li" />

      <ListItem alignItems="flex-start">
        <ListItemAvatar>
          <Skeleton variant="circular" width={40} height={40} />
        </ListItemAvatar>
        <ListItemText sx={{ display: "flex", flexDirection: "row" }}>
          <Skeleton variant="rectangular" width={150} height={20} sx={{ marginBottom: 2 }} />
          <Skeleton variant="rectangular" width={400} height={20} sx={{ marginBottom: 1 }} />
          <Skeleton variant="rectangular" width={300} height={20} />
        </ListItemText>
      </ListItem>

      <Divider variant="inset" component="li" />

      <ListItem alignItems="flex-start">
        <ListItemAvatar>
          <Skeleton variant="circular" width={40} height={40} />
        </ListItemAvatar>
        <ListItemText sx={{ display: "flex", flexDirection: "row" }}>
          <Skeleton variant="rectangular" width={150} height={20} sx={{ marginBottom: 2 }} />
          <Skeleton variant="rectangular" width={400} height={20} sx={{ marginBottom: 1 }} />
          <Skeleton variant="rectangular" width={300} height={20} />
        </ListItemText>
      </ListItem>
      
      <Divider variant="inset" component="li" />

      <ListItem alignItems="flex-start">
        <ListItemAvatar>
          <Skeleton variant="circular" width={40} height={40} />
        </ListItemAvatar>
        <ListItemText sx={{ display: "flex", flexDirection: "row" }}>
          <Skeleton variant="rectangular" width={150} height={20} sx={{ marginBottom: 2 }} />
          <Skeleton variant="rectangular" width={400} height={20} sx={{ marginBottom: 1 }} />
          <Skeleton variant="rectangular" width={300} height={20} />
        </ListItemText>
      </ListItem>

      <Divider variant="inset" component="li" />

      <ListItem alignItems="flex-start">
        <ListItemAvatar>
          <Skeleton variant="circular" width={40} height={40} />
        </ListItemAvatar>
        <ListItemText sx={{ display: "flex", flexDirection: "row" }}>
          <Skeleton variant="rectangular" width={150} height={20} sx={{ marginBottom: 2 }} />
          <Skeleton variant="rectangular" width={400} height={20} sx={{ marginBottom: 1 }} />
          <Skeleton variant="rectangular" width={300} height={20} />
        </ListItemText>
      </ListItem>

      <Divider variant="inset" component="li" />

      <ListItem alignItems="flex-start">
        <ListItemAvatar>
          <Skeleton variant="circular" width={40} height={40} />
        </ListItemAvatar>
        <ListItemText sx={{ display: "flex", flexDirection: "row" }}>
          <Skeleton variant="rectangular" width={150} height={20} sx={{ marginBottom: 2 }} />
          <Skeleton variant="rectangular" width={400} height={20} sx={{ marginBottom: 1 }} />
          <Skeleton variant="rectangular" width={300} height={20} />
        </ListItemText>
      </ListItem>
    </List>
  )
}