import React, { useState, useEffect } from "react";
import ComponentSlider from "./Carousel/ComponentSlider";
import { Typography, Box, Grid, Paper } from "@mui/material";
import { useKeycloak } from "@react-keycloak/web";
import lists from "../../../img/lists.jpg";
import detail from "../../../img/detail.png";
import Tile1 from "../../../img/Tile1_HomePage.png";
import Tile2 from "../../../img/Tile2_HomePage.png";
import Tile3 from "../../../img/Tile3_HomePage.png";
import Tile4 from "../../../img/Tile4_HomePage.png";
import Top from "../../../img/Top_HomePage.png";
const Cloak = () => {
  const { keycloak, initialized } = useKeycloak();
  var [featuredCards, setFeatures] = useState(null);
  const [favorites, setFavorites] = useState([]);
  const [loggedInUser, setLoggedInUser] = useState({});

  let keycloakUsername = keycloak.idTokenParsed?.preferred_username;

  useEffect(() => {
    getLoggedInUser();
  }, [keycloakUsername]);

  useEffect(() => {
    getFavorites();
  }, [loggedInUser]);

  useEffect(() => {
    getFeatures();
  }, []);

  const getLoggedInUser = async () => {
    if (!keycloakUsername) return;

    const url = `/api/users/username/${keycloakUsername}`;
    const response = await fetch(url);
    const data = await response.json();
    setLoggedInUser(data.user);

    return data.user;
  };

  const getFeatures = async () => {
    let res = await fetch("/api/cards/random/10");
    let featuredCards = await res.json();
    setFeatures(featuredCards.cards);
  };

  const getFavorites = async () => {
    if (!loggedInUser || Object.keys(loggedInUser).length === 0) return; //null and empty check to avoid unnecessary calls

    const url = `/api/collections/favorites/${loggedInUser.userid}`;
    const response = await fetch(url);
    const data = await response.json();

    setFavorites(data.cards);
  };

  const Favorites = () => {
    if (favorites.length < 2) {
      return <Box> </Box>;
    } else {
      return (
        <Box>
          <Typography
            variant="h4"
            component="h4"
            sx={{ textAlign: "center", marginLeft: "25px" }}
          >
            My Favorites
          </Typography>
          <ComponentSlider cardsList={favorites} />
        </Box>
      );
    }
  };

  return (
    <div>
      <Box>
          <Box sx={{ marginBottom: "40px" }}>
            <Typography
              variant="h4"
              component="h4"
              sx={{ textAlign: "center", marginLeft: "25px" }}
            >
              Featured cards
            </Typography>
            <ComponentSlider cardsList={featuredCards} />
          </Box>
          <Box>
            <Favorites />
          </Box>
      </Box>
    </div>
  );
};


const Home = () => {
  const { keycloak, intialized } = useKeycloak();

  return (
    <div height="auto" width="50%">
      {keycloak.authenticated && (
        <Box sx={{width: "75%", marginLeft: "auto", marginRight: "auto"}}>
            <Cloak sx={{width: "50%"}} />
        </Box>
      )}
      {/* Render this page if no user is logged in (Default home page) */}
      {!keycloak.authenticated && (
        <Grid container spacing={0} columns={12} sx={{ width: "100%" }}>
          <Box sx={{width: "100%", float: "center"}}>
            <Grid item xs={12}>
              <img src={Top} alt={'Become a Master'} style={{ display: 'block', margin: 'auto', width: '80%' }}/>
            </Grid>
          </Box>
          <Box sx={{ backgroundColor: "white", width: "100%" }}>
            <Grid item xs={12} container sx={{ paddingTop: "80px", paddingBottom: "80px", alignItems: "center" }}>
              <Grid item xs={7} md={8}>
                <img alt={'Search and Browse'} src={Tile1} style={{ display: 'block', margin: 'auto', width: '100%' }} />
              </Grid>
              <Grid item xs={5} md={4}>
                <Box sx={{ width: "85%", float: "left" }}>
                  <Typography variant="h3"
                    component="h3"
                    sx={{ textAlign: "left", fontWeight: 400, color: "black" }}>
                    Search for your favourite Pokemon Cards
                  </Typography>
                  <Typography variant="h5"
                    sx={{ textAlign: "left", color: "black" }}>
                    Cardlogue offers thousands of Pokemon cards for you to browse and filter through to find your favourite.
                  </Typography>
                </Box>
              </Grid>
            </Grid>
          </Box>

          <Box sx={{ backgroundColor: "#f5f5f5", width: "100%" }}>
            <Grid item xs={12} container sx={{ paddingTop: "80px", paddingBottom: "80px", alignItems: "center" }}>
              <Grid item xs={6} md={5}>
                <Box sx={{ width: "75%", float: "right" }}>
                  <Typography variant="h3"
                    component="h3"
                    sx={{ textAlign: "right", fontWeight: 400, color: "black" }}>
                    View details on your favourite Pokemon Cards
                  </Typography>
                  <Typography variant="h5"
                    sx={{ textAlign: "right", color: "black" }}>
                    Read up on your favourite Pokemon cards, their typing, evolutions and much more!
                  </Typography>
                </ Box>
              </Grid>
              <Grid item xs={6} md={7}>
                <img alt={'Details'} src={Tile2} style={{ display: 'block', margin: 'auto', width: '100%' }} />
              </Grid>
            </Grid>
          </Box>

          <Box sx={{ backgroundColor: "white", width: "100%" }}>
            <Grid item xs={12} container sx={{ paddingTop: "80px", paddingBottom: "80px", alignItems: "center" }}>
              <Grid item xs={7} md={8}>
                <img alt={'Comment and Rate'} src={Tile3} style={{ display: 'block', margin: 'auto', width: '100%' }} />
              </Grid>
              <Grid item xs={5} md={4}>
                <Box sx={{ width: "85%", float: "left" }}>
                  <Typography variant="h3"
                    component="h3"
                    sx={{ textAlign: "left", fontWeight: 400, color: "black" }}>
                    Rate and Comment on your favourite Pokemon Cards
                  </Typography>
                  <Typography variant="h5"
                    sx={{ textAlign: "left", color: "black" }}>
                    You can rate and comment on your favourite pokemon cards, letting others know your thoughts.
                  </Typography>
                </Box>
              </Grid>
            </Grid>
          </Box>

          <Box sx={{ backgroundColor: "#f5f5f5", width: "100%" }}>
            <Grid item xs={12} container sx={{ paddingTop: "80px", paddingBottom: "80px", alignItems: "center" }}>
              <Grid item xs={6} md={5}>
                <Box sx={{ width: "75%", float: "right" }}>
                  <Typography variant="h3"
                    component="h3"
                    sx={{ textAlign: "right", fontWeight: 400, color: "black" }}>
                    Create your own Collections
                  </Typography>
                  <Typography variant="h5"
                    sx={{ textAlign: "right", color: "black" }}>
                    Create unique collections and add some of your favourite Pokemon Cards. Collect over 14,000 cards.
                  </Typography>
                </Box>
              </Grid>
              <Grid item xs={6} md={7}>
                <img alt={'Collect'} src={Tile4} style={{ display: 'block', margin: 'auto', width: '100%' }} />
              </Grid>
            </Grid>
          </Box>

        </Grid>
      )}
    </div>
  );

}

class HomePage extends React.Component {
  render() {
    return (
      <div>
        <Home />
      </div>
    );
  }
}

export default HomePage;
