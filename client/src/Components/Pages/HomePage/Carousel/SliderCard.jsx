import React, { useState, useEffect } from 'react';
import Paper from '@mui/material/Paper';
import { Link } from 'react-router-dom';
import ImageFetch from "../../../../Helpers/imageFetch"; 

import 'swiper/css';

export default function SliderCard(props) {
  const [imgUrl, setImg] = useState("https://pokemonblobs.blob.core.windows.net/images/undefined/undefined");

  async function checkURL(setID, cardID) {
    var imgUrl =  `https://pokemonblobs.blob.core.windows.net/images/${setID}/${cardID}`;
    var http = new XMLHttpRequest();
    http.open("HEAD", imgUrl, false);
    http.setRequestHeader("Access-Control-Allow-Origin", "*");
    http.send();
    if (http.status !== 200) {
      // page did not load
      ImageFetch(setID, cardID).then(apiURL => setImg(apiURL));
    } else {
      setImg(imgUrl);
    }
  }

  useEffect(() => {checkURL(props.card.setid, props.card.cardid.split("-")[1]); }, []);
  
  return (
    props.noLink ? 
      <Paper elevation={3} sx={{ m: 1, marginLeft: "-100px", width: 128, height: 180 }} >
        <img alt="card" src={imgUrl} style={{width:'100%', height:'auto'}} />
      </Paper> :
      <Link to={`/card/${props.card.cardid}`} style={{ textDecoration: 'none' }}>
        <Paper elevation={3} sx={{ m: 1, width: 128, height: 180 }} >
          <img alt="card" src={imgUrl} style={{width:'100%', height:'auto'}} />
        </Paper>
      </Link>
      
  )
}