import React from 'react';
import { Swiper, SwiperSlide } from "swiper/react";
import SwiperCore, { Navigation, Pagination} from 'swiper/core';
import "swiper/css/pagination";
import "swiper/css/navigation";
import Box from '@mui/material/Box';
import Skeleton from '@mui/material/Skeleton';
import SliderCard from './SliderCard.jsx';

SwiperCore.use([Pagination, Navigation]);

export default function ComponentSlider(props) {
  return (
    <Box
      sx={{
        display: 'flex',
        width: '100%',
        flexWrap: 'wrap',
        backgroundColor: '#2d3a40', // slate grey
        margin: '10px',
        borderRadius: "5px",
        paddingRight: "5px"
      }}
    >
      <Swiper 
        loop={true}
        lazy={true}
        spaceBetween={3} 
        slidesPerView={5}
        mousewheel={true}
        navigation={true}
        pagination={{
          clickable: true
        }}
      >
        {
          props.cardsList ? (
            props.cardsList.map((value, index) => (
              <SwiperSlide>
                <SliderCard card={value} />
              </SwiperSlide>
            ))
          ) : (
            [...Array(10),].map((value, index) => (
              <SwiperSlide>
                <Skeleton variant="rectangular" sx={{ m: 1, borderRadius: "5px" }} width={128} height={180} />
              </SwiperSlide>
            ))
          )
        }
      </Swiper>
    </Box>
  )
}
