import React, {useEffect, useState} from 'react';
import Box from '@material-ui/core/Box';
import DraggableList from './DraggableList';
import { getItems, reorder } from './helpers';
import { fetchCollectionsFromUser } from "../Controls/CollectionsControl";
import { useKeycloak } from "@react-keycloak/web";

const ListsPage = () => {
  const { keycloak, initialized } = useKeycloak();

  const [collections, setCollections] = useState([]);
  let keycloakUsername = keycloak.idTokenParsed?.preferred_username;

  useEffect(() => {
    // need to get the username from keycloak obj
    if(!keycloakUsername) return;
    
    fetchCollectionsFromUser(keycloakUsername).then((collections) => setCollections(collections));
  },[keycloakUsername]);

  const onDragEnd = ({ destination, source }) => {
    // dropped outside the list
    if (!destination) return;

    const newItems = reorder(collections, source.index, destination.index);

    setCollections(newItems);
    
    for(let i = 0; i < newItems.length; i++) {
      updateCollectionRank(newItems[i], i);
    }
  };

  const updateCollectionRank = async (collection, rank) => {
    const url = `/api/collections/rank/${collection.collectionid}`

    const body = { 
      "rank": rank
    }

    const requestParams = {
      method: "PUT",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(body)
    }

    const data = await fetch(url, requestParams);

  }

  return (
      <Box sx={{mx: "20vh", my: "10vh"}}>
        <DraggableList items={collections} onDragEnd={onDragEnd} />
      </Box>  
  );
};

export default ListsPage;
