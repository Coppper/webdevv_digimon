import React, { useState, useEffect } from "react";
import { Draggable } from 'react-beautiful-dnd';
import { Link } from "react-router-dom";

import makeStyles from '@material-ui/core/styles/makeStyles';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import InboxIcon from '@material-ui/icons/Inbox';
import SliderCard from "./../HomePage/Carousel/SliderCard"
import { Paper } from "@mui/material";

const useStyles = makeStyles({
  draggingListItemDrag: {
    background: '#526e85',
    borderRadius: '5px',
    padding: '40px'
  },

  draggingListItem: {
    background: '#274966a6', // darkish blue
    margin: "5px 0",
    borderRadius: '5px',
    paddingLeft: '40px'
  },

  listItemText:{
    fontSize:'2.5em',//Insert your required size,
    fontVariantCaps: "all-petite-caps",
    letterSpacing: "1px"
  }
});

const DraggableListItem = ({ item, index }) => {
  const [cards, setCards] = useState([]);

  useEffect(async () => {
    if(!item) return;

    const url = `/api/collection/${item.collectionid}`;
    const res = await fetch(url);
    const data = await res.json();

    setCards(data.collections[0].collectionbridges[0].cards)
  }, [item])

  const classes = useStyles();
  return (
    <Paper>
    <Draggable draggableId={item.collectionid + ""} index={index}>
      {(provided, snapshot) => (
        <Link to={`/collection/${item.collectionid}`} style={{textDecoration: "none", color: "white"}}>
          <Paper>
          <ListItem
            ref={provided.innerRef}
            {...provided.draggableProps}
            {...provided.dragHandleProps}
            className={snapshot.isDragging ? classes.draggingListItemDrag : classes.draggingListItem }
          >
            <ListItemText classes={{primary: classes.listItemText}} primary={item.collectionname} />
              { cards.map((value, index) => (
                <SliderCard key={index} card={value} noLink={true} />
              )) }              
          </ListItem>
          </Paper>
        </Link>
      )}
    </Draggable>
    </Paper>
  );
};

export default DraggableListItem;