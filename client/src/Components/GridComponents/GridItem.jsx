import React, { } from 'react';
import { Grid, Button, Paper, Box, Skeleton } from '@mui/material';
import styles from "../Pages/CollectionsPage/DetailCardPage.module.css";

/**
 * Maps the new position of the card
 * 
 * @param {*} val 
 * @param {*} minA 
 * @param {*} maxA 
 * @param {*} minB 
 * @param {*} maxB 
 * @returns coordinates of the hovered component
 */
function map(val, minA, maxA, minB, maxB) {
    return minB + ((val - minA) * (maxB - minB)) / (maxA - minA);
}

/**
 * Component that renders a single cards image that is clickable
 * 
 * @param {*} props 
 * @returns Grid item of a card
 */
export default function BrowseGridItem(props) {
    //contains the state of hovered or not hovered
    const [CardState, setCardState] = React.useState({
        xRotation: 0,
        yRotation: 0,
        brightness: 1,
    });


    /**
    * Handles the mouse hover event to add an effect 
    * to the item that is hovered over
    * 
    * @param {*} e 
    */
    function handleMouseHover(e) {
        let mouseX = e.nativeEvent.offsetX;
        let mouseY = e.nativeEvent.offsetY;
        let brightness = map(mouseY, 0, 250, 1.2, 0.8);

        setCardState({
            brightness,
            scale: 1.3,
        });
    }

    /**
     * Handles the event of the mouse no longer hovering 
     * over an item, removing the hover effect
     */
    function handleMouseLeave() {
        setCardState({
            brightness: 1,
            scale: 1,
        });
    }

    return (
        <Grid item xs={4} md={3} lg={2} >
            <Box>
                <Paper
                    className={styles.card}
                    elevation={3}
                    onMouseLeave={() => handleMouseLeave()}
                    onMouseMove={(e) => handleMouseHover(e)}
                    sx={{
                        m: 1,
                        borderRadius: "10px",
                        transform: `scale(${CardState.scale})`,
                        filter: `brightness(${CardState.brightness})`,
                    }}
                >
                    <Button onClick={props.onClick}>
                        <img alt='card' src={props.value.cardapilink} style={{ height: "auto", width: '100%' }} />
                    </Button>
                </Paper>
            </Box>

        </Grid>
    );

}