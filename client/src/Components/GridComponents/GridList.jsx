import * as React from 'react';
import { Grid, Box } from '@mui/material';
import GridItem from './GridItem';

/**
 * Component renders list of objects passed through props
 * 
 * @param {*} props 
 * @returns Box holding a Grid of GridItem
 */
export default function GridList(props){
    //stores a list of cards
    const [data, setData] = React.useState([]);

    React.useEffect(() => {
        setData(props.cards)
    }, [props])

    return (
        <Box
            sx={{
                borderRadius: 1.5,
                boxShadow: "1px 1px 8px 4px rgba(33,56,74,0.84)"
            }}
        >
            <Grid container spacing={0} column={10}>
                {data.map((card) => (
                    <GridItem value={card} onClick={() => props.onClick(card.cardid)}/>
                ))}

            </Grid>
        </Box>

    );
}