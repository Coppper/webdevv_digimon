/* eslint-disable */

import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { ThemeProvider, createTheme } from '@mui/material'; 
import CssBaseline from '@mui/material/CssBaseline'

export const theme = createTheme({
  palette: {
    primary: {
      main: '#af4b4d',
      dark: '#a08181',
    },
    secondary: {
      main: '#5587bf',
    },
    info: {
      main: '#002d54',
    },
    type: 'dark',
    text: {
      primary: 'rgba(255,255,255,0.87)',
      secondary: 'rgba(255,255,255,0.54)',
      disabled: 'rgba(255,255,255,0.38)',
      hint: 'rgba(255,255,255,0.38)',
    },
    background: {
      paper: '#252525',
      default: '#181818',
    },
  },
});

ReactDOM.render(
  <React.StrictMode>
    <ThemeProvider theme={theme}>
    <CssBaseline />
    <App />
    </ThemeProvider>
  </React.StrictMode>,
  document.getElementById('root')
);