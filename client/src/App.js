import './App.css';
import Navbar from './Components/Nav/Navbar.jsx';
import HomePage from './Components/Pages/HomePage/HomePage.jsx';
import ProfilePage from './Components/Pages/ProfilePage/ProfilePage.jsx';
import BrowsePage from './Components/Pages/BrowsePage/BrowsePage.jsx';
import ListsPage from './Components/Pages/ListsPage/ListsPage';
import CollectionsPage from './Components/Pages/CollectionsPage/CollectionsPage.jsx';
import { BrowserRouter as Router, Routes, Route, Navigate } from "react-router-dom";
import { ReactKeycloakProvider } from "@react-keycloak/web";
import DetailCardPage from './Components/Pages/DetailsPage/DetailCardPage.jsx';
import keycloak from './Authentication/keycloak.jsx';
import SnackbarAlert from './Components/Pages/SnackbarAlert';
import { SnackbarContainer } from './context/SnackbarContext';
import Error from './Error'

function App() {
  return (
    <SnackbarContainer>
    <div className="App">
      <ReactKeycloakProvider authClient={keycloak}>
      <Router>
        <Navbar />
        <Routes>
          <Route path="/" element={<HomePage />}/>
          <Route path="/profile" element={<ProfilePage />}/>
          <Route path="/browse/:page" element={<BrowsePage />}/>          
          <Route path="/card/:id" element={<DetailCardPage/>}/>
          <Route path="/lists" element={<ListsPage />}/>
          <Route path="/profile/:username" element={<ProfilePage />}/>
          <Route path="/collection/:id" element={<CollectionsPage/>}/>
          <Route path="*" element={<Error />}/>
        </Routes>
      </Router>
      </ReactKeycloakProvider>
      <SnackbarAlert/>
    </div>
    </SnackbarContainer>
  );
}

export default App;