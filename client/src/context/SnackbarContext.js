import React, { createContext, useState } from 'react';

/**
 * Handler that will be used to control the snackbar's state
 */
export const SnackbarContext = createContext({
    setSnackbar: () => {
    },
    snackbar: {}
});
/**
 * The 'container' for the Snackbar component
 * @param {*} props 
 * @returns 
 */
export const SnackbarContainer = ({ children }) => {
    const [snackbar, setSnackbar] = useState({
        message: "",
        severity: ""
    });

    // other components will be able to set snackbar
    const handleSnackbarSet = (message, severity) => {
        setSnackbar({
            message, severity
        })
    };

    const contextValue = {
        setSnackbar: handleSnackbarSet,
        snackbar
    };

    return (
        <SnackbarContext.Provider value={contextValue}>
            {children}
        </SnackbarContext.Provider>
    )
};