import Keycloak from "keycloak-js";
const keycloak = new Keycloak({
    url: "https://merckauth.azurewebsites.net/auth",
    realm: "pokemonlogins",
    clientId: "react-login",
});

export default keycloak;