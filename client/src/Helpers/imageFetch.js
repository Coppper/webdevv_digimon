const { BlobServiceClient } = require("@azure/storage-blob");
const fetch = require("node-fetch");
var Buffer = require('buffer');

async function main(set, id) {
  const STORAGE_CONNECTION_STRING =
    process.env.STORAGE_CONNECTION_STRING || "BlobEndpoint=https://pokemonblobs.blob.core.windows.net/;QueueEndpoint=https://pokemonblobs.queue.core.windows.net/;FileEndpoint=https://pokemonblobs.file.core.windows.net/;TableEndpoint=https://pokemonblobs.table.core.windows.net/;SharedAccessSignature=sv=2020-08-04&ss=bfqt&srt=sco&sp=rwdlacupitfx&se=2022-06-11T12:19:32Z&st=2022-02-14T05:19:32Z&spr=https&sig=CNwFFXUwJWIE3mCyFFRhy%2Br7Tvd%2Bo6PyzJOXzRJ6EDM%3D";
  const blobServiceClient = BlobServiceClient.fromConnectionString(
    STORAGE_CONNECTION_STRING
  );
  const containerClient = blobServiceClient.getContainerClient("images");

  try {
    const blockBlobClient = containerClient.getBlockBlobClient(`${set}/${id}`);
    return fetchImage(set, id, blockBlobClient);
  } catch (error) {
  }
}

/*
* This method will fetch the image from the pokemon api
* only when we DO NOT have a copy already stored in our 
* blob storage as to avoid ddosing the api
*/
function fetchImage(set, id, blobBlock) {
  if(!set || !id) return null;

  fetch(`https://images.pokemontcg.io/${set}/${id}.png`)
    .then(function (response) {
      return response.blob();
    })
    .then(function (blob) {
      uploadImage(blob, blobBlock);
    }).then(function (){
      updateImage(set, id);
    }
    );
    return `https://images.pokemontcg.io/${set}/${id}.png`;
}

/*
* This method converts our blob into a buffer to
* facilitate uploading to our blob storage as well
* setting the blobType to an image
*/
async function uploadImage(blob, blobBlock) {
  const blobOptions = { blobHTTPHeaders: { blobContentType: 'image/png' }};
  var arrayBuffer = await blob.arrayBuffer();
  var buffer = Buffer.Buffer.from(arrayBuffer);
  blobBlock.upload(blob, buffer.length, blobOptions);
}

/**
 * This method sends an update request to our api to
 * update our database with the api link to limit
 * the amount of http requests we make
 * @param {*} set 
 * @param {*} id 
 */
function updateImage(set, id){
  if(!set || !id) return;

  const imgId = set + "-" + id;
  const imgLink = {cardapilink: `https://pokemonblobs.blob.core.windows.net/images/${set}/${id}`};

  fetch(`/api/cards/upload/${imgId}`, {
    method: 'POST', 
    mode: 'same-origin',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(imgLink),
  })
  .then(response => response.json())
  .then(data => {
  })
  .catch((error) => {
  });
}


main().catch((err) => {
  console.error("Unable to connect to container:", err.message);
});


export default main;