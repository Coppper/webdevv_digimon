
/**
 * This is a helper function that takes in
 * a keycloak object and users the username from 
 * it to send a fetch to our API adding them to our
 * azure db if it doesn't already exist
 * @param {Keycloak object} input 
 * @returns 
 */
const addUser = async (input) => {
  const username = { username: input.tokenParsed.preferred_username };
  let getUser = "/api/users";
  // this adds the user to the database (just their username)
  const request = await fetch(getUser, {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(username),
  });
  if (request.ok) {
    return true;
  }
}; 

export default addUser;
